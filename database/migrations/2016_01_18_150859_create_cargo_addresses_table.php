<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCargoAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargo_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shipper_id');
            $table->string('address');
            $table->integer('city_id');
            $table->integer('township_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cargo_addresses');
    }
}
