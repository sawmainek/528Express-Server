<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_company', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('company_no',100);
            $table->string('address');
            $table->integer('city_id');
            $table->integer('township_id');
            $table->string('phone',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('delivery_company');
    }
}
