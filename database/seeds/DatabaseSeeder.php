<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Deliverycompany',
            'slug' => 'deliverycompany',
            'permissions' => [
                            
            ]
        ]);
    }
}
