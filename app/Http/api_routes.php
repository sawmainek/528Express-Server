 <?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('/admin/login');
});

// Route::filter('Sentinel', function()
// {
// 	if ( ! Sentinel::check()) {
//  		return Redirect::to('admin/signin')->with('error', 'You must be logged in!');
//  	}
// });

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['prefix' => 'api','middleware' => ['web']], function () {
    Route::get('register','Admin\AdminController@getRegister');
    Route::post('register','Admin\AdminController@postRegister');
    Route::get('login','Admin\AdminController@getLogin');
    Route::post('login','Admin\AdminController@postLogin');
    Route::get('logout','Admin\AdminController@getLogout');

     
	Route::get('dashboard','Admin\HomeController@getIndex');

	//Route::group(['middleware' => ['web','sentinel.admin']], function () {
		/*For Activation */
		Route::get('activate/{userId}/{activationCode}',['as' => 'activate', 'uses' => 'Admin\AdminController@getActivate']);
		Route::get('resend/{phone}',['as' => 'resend', 'uses' => 'Admin\AdminController@getResend']);
		/* for user */
		Route::resource('user','Admin\UserController');

		Route::post('cargocalculate','Admin\CargocalculateController@index');

		/* for delivery */
		//Route::get('deliverycompany/editcity/{id}',['as' => 'deliverycompany/editcity', 'uses' =>'Admin\DeliverycompanyController@editCity']);
		Route::resource('deliverycompany','Admin\DeliverycompanyController');
		Route::get('staffslist/{id}',['as' => 'staffslist', 'uses' =>'Admin\DeliverystaffController@getlist']);
		Route::resource('deliverystaff','Admin\DeliverystaffController');
		Route::resource('deliverytransaction','Admin\DeliverytransactionController');
		Route::resource('deliverystafflocation','Admin\DeliverystafflocationController');
		Route::resource('deliverylocation','Admin\DeliverylocationController');
		Route::get('addstaffs/{id}/{userId}',['as' => 'addstaffs', 'uses' =>'Admin\MylistController@getapiaddstaff']);
		Route::post('addstaffs/{id}','Admin\MylistController@postAddstaff');
		Route::post('accept/{id}','Admin\MylistController@postAccept');
		Route::post('cancel/{id}','Admin\MylistController@postCancel');

		/* for Shipper */
		Route::resource('shipping','Admin\ShippingController');
		Route::post('confirm-shipping/{id}','Admin\ConfirmShippingController@postConfirmshipping')->middleware('throttle:1');
		// Route::resource('shipper','Admin\ShipperController');
		// Route::resource('shippinginformation','Admin\ShippinginformationController');

		/*For Shippging list by Shipper*/
		Route::get('shippingaddresslist/{id}',['as' => 'shippingaddresslist', 'uses' =>'Admin\MylistController@getShipperaddresslist']);
		Route::get('cargoaddresslist/{id}',['as' => 'cargoaddresslist', 'uses' =>'Admin\MylistController@getCargoaddresslist']);
		Route::get('cargolist/{id}',['as' => 'cargolist', 'uses' =>'Admin\MylistController@getCargolist']);
		Route::get('shippinglist/{id}',['as' => 'shippinglist', 'uses' =>'Admin\MylistController@getShippinglist']);
		Route::get('notsendinglist/{id}',['as' => 'notsendinglist', 'uses' =>'Admin\MylistController@getNotsendinglist']);
		Route::get('pendinglist/{id}',['as' => 'pendinglist', 'uses' =>'Admin\MylistController@getPendinglist']);
		Route::get('completelist/{id}',['as' => 'completelist', 'uses' =>'Admin\MylistController@getCompletelist']);
		Route::get('tracking/{code}',['as' => 'tracking', 'uses' =>'Admin\MylistController@getTracking']);
		Route::post('complete/{id}','Admin\MylistController@postComplete');
		Route::post('receive/{id}','Admin\MylistController@postReceive');
		Route::get('trackshipping/{code}',['as' => 'trackshipping', 'uses' =>'Admin\MylistController@getTrackshipping']);
		Route::get('shippingcount/{id}',['as' => 'shippingcount', 'uses' =>'Admin\MylistController@getShippingcount']);
		Route::get('editshipping/{id}',['as' => 'editshipping', 'uses' =>'Admin\MylistController@getEditdelivery']);
		Route::post('editshipping/{id}',['as' => 'editshipping', 'uses' =>'Admin\MylistController@postEditdelivery']);

		Route::get('neworderlists/{id}',['as' => 'neworderlists', 'uses' =>'Admin\MylistController@apineworderlist']);
		Route::get('acceptorderlists/{id}',['as' => 'acceptorderlists', 'uses' =>'Admin\MylistController@apiAcceptorderlist']);
		Route::get('receiveorderlists/{id}',['as' => 'receiveorderlists', 'uses' =>'Admin\MylistController@apiReceiveorderlist']);
		Route::get('completeorderlists/{id}',['as' => 'completeorderlists', 'uses' =>'Admin\MylistController@apicompleteorderlist']);
		/* for city */
		Route::resource('city','Admin\CityController');

		/* for country */
		Route::resource('country','Admin\CountryController');

		/* for township */
		Route::resource('township','Admin\TownshipController');

		/*For Image Upload*/
		Route::post('imageupload','Admin\ImageController@index');

		/*For Andriod Version*/
		Route::get('version','Admin\VersionController@getVersion');
	

	//}); 
	
});



 