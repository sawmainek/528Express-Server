<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Cargo;
use App\Shipper;
use Sentinel;
use Config;
use App\User;
use Mail;
use App\Delivery_staff;
use App\Cargo_address;
use App\Delivery_transaction;
use App\Shipping_transaction;
use App\Shipping_information;
use App\Shipping_transaction_detail;
use App\Http\Requests;
use App\City;
use App\Township;
use App\Delivery_city;
use Validator,ErrorException;
use DB;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class MylistController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
         public function getShippinglist(Request $request,$id)
         {
            $response = null;
            $shipper = Shipper::where('user_id',$id)->first();
            $shipping_transaction1 = Shipping_transaction::where('shipper_id',$shipper->id)->where('status',0)->get();
            $notshippinglist = count($shipping_transaction1);
            $response['notshippinglist'] = $notshippinglist;

            $shipping_transaction2 = Shipping_transaction::where('shipper_id',$shipper->id)->where('status',1)->get();
            $pendinglist = count($shipping_transaction2);
            $response['pendinglist'] = $pendinglist;

            $shipping_transaction3 = Shipping_transaction::where('shipper_id',$shipper->id)->where('status',2)->get();
            $completelist = count($shipping_transaction3);
            $response['completelist'] = $completelist;

            return response()->json($response);
         }

         public function getShippingcount(Request $request,$id)
         {
            $response = null;
            $users = Sentinel::findById($id);
            $user = Delivery_staff::where('user_id',$users->id)->first();
            if ($users->inRole('deliverycompany'))
            {
                 $orderlists = Delivery_transaction::where('delivery_company_id',$user->delivery_company_id)->orderBy('created_at', 'desc')->where('delivery_staff_id',0)->get();
                 $neworderlist = count($orderlists);
                 $response['neworderlist'] = $neworderlist;
            }
            else{
                 $orderlists = Delivery_transaction::where('delivery_staff_id',$user->id)->where('status',"pending")->orderBy('created_at', 'desc')->get();   
                 $neworderlist = count($orderlists);
                 $response['neworderlist'] = $neworderlist;
            }

             if ($users->inRole('deliverycompany'))
            {
                 $orderlists = Delivery_transaction::where('delivery_company_id',$user->delivery_company_id)->orderBy('created_at', 'desc')->where('status',"accept")->get();
                 $acceptorderlist = count($orderlists);
                 $response['acceptorderlist'] = $acceptorderlist;
            }
            else{
                $orderlists = Delivery_transaction::where('delivery_staff_id',$user->id)->where('status',"accept")->orderBy('created_at', 'desc')->get();   
                $acceptorderlist = count($orderlists);
                $response['acceptorderlist'] = $acceptorderlist;
            } 
            if ($users->inRole('deliverycompany'))
            {
                 $orderlists = Delivery_transaction::where('delivery_company_id',$user->delivery_company_id)->orderBy('created_at', 'desc')->where('status',"receive")->get();
                 $receiveorderlist = count($orderlists);
                 $response['receiveorderlist'] = $receiveorderlist;
            }
            else{
                $orderlists = Delivery_transaction::where('delivery_staff_id',$user->id)->where('status',"receive")->orderBy('created_at', 'desc')->get();   
                $receiveorderlist = count($orderlists);
                $response['receiveorderlist'] = $receiveorderlist;
            }
            if ($users->inRole('deliverycompany'))
            {
                 $orderlists = Delivery_transaction::where('delivery_company_id',$user->delivery_company_id)->orderBy('created_at', 'desc')->where('status',1)->get();
                 $completeorderlist = count($orderlists);
                 $response['completeorderlist'] = $completeorderlist;
            }
            else{
                $orderlists = Delivery_transaction::where('delivery_staff_id',$user->id)->orderBy('created_at', 'desc')->where('status',1)->get();   
                $completeorderlist = count($orderlists);
                $response['completeorderlist'] = $completeorderlist;
            }
            return response()->json($response);
         }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function getNotsendinglist(Request $request,$id)
        {
        	$shipper = Shipper::where('user_id',$id)->first();
        	
        	$lists = Shipping_transaction::with('shipping_information','shipping_transaction_detail','delivery_company')->where('shipper_id',$shipper->id)->where('status',0)->get();
        	foreach ($lists as $key => $list) {
        		foreach ($list->shipping_transaction_detail as $i => $value) {
        			 $cargo = Cargo::where('id',$value->cargo_id)->first();
        			 $lists[$key]['shipping_transaction_detail'][$i]['cargo'] = $cargo;
        		}
        	}
        	return response()->json($lists);
        }
        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function getPendinglist(Request $request,$id)
        {
            $shipper = Shipper::where('user_id',$id)->first();
            
            $lists = Shipping_transaction::with('shipping_information','shipping_transaction_detail','delivery_company')->where('shipper_id',$shipper->id)->where('status',1)->get();
            foreach ($lists as $key => $list) {
                $delivery = Delivery_transaction::where('shipping_transaction_id',$list->id)->first();
                $lists[$key]['delivery_transaction'] = $delivery;
                foreach ($list->shipping_transaction_detail as $i => $value) {
                     $cargo = Cargo::where('id',$value->cargo_id)->first();
                     $lists[$key]['shipping_transaction_detail'][$i]['cargo'] = $cargo;
                }
            }
            return response()->json($lists);
        }

        public function getCompletelist(Request $request,$id)
        {
            $shipper = Shipper::where('user_id',$id)->first();
            
            $lists = Shipping_transaction::with('shipping_information','shipping_transaction_detail','delivery_company')->where('shipper_id',$shipper->id)->where('status',2)->get();
            foreach ($lists as $key => $list) {
                foreach ($list->shipping_transaction_detail as $i => $value) {
                     $cargo = Cargo::where('id',$value->cargo_id)->first();
                     $lists[$key]['shipping_transaction_detail'][$i]['cargo'] = $cargo;
                }
            }
            return response()->json($lists);
        }
        /**
         * Display the specified resource.
         *
         * 
         * @return \Illuminate\Http\Response
         */
        public function getOrderlist(Request $request)
        {
            $id = Sentinel::getUser()->id;
            $user = Delivery_staff::where('user_id',$id)->first();

            $orderlist = Delivery_transaction::where('delivery_company_id',$user->delivery_company_id)->get();
                         
        }


        /**
         * Display the specified resource.
         *
         * For Admin Panel
         * @return \Illuminate\Http\Response
         */
        public function getneworderlist()
        {
            $users = Sentinel::getUser();
            $user = Delivery_staff::where('user_id',$users->id)->first();
            if ($users->inRole('deliverycompany'))
            {
                 $orderlists = Delivery_transaction::with('delivery_staff')->where('delivery_staff_id',0)->where('delivery_company_id',$user->delivery_company_id)->orwhere('status','pending')->where('delivery_company_id',$user->delivery_company_id)->orderBy('created_at','desc')->get();
                 foreach ($orderlists as $key => $orderlist) {
                    $trans = Shipping_transaction::with('shipper','delivery_company','cargo_address','shipping_information','shipping_transaction_detail')->where('id',$orderlist->shipping_transaction_id)->get();
                        foreach ($trans as $i => $tran) {
                            foreach ($tran->shipping_transaction_detail as  $j=>$value) {
                                 $cargo = Cargo::where('id',$value->cargo_id)->first();
                                 $trans[$i]['shipping_transaction_detail'][$j]['cargo'] = $cargo;
                            }   
                            $shipper = User::where('id',$tran->shipper->user_id)->first(); 
                            $trans[$i]['shipper']['user']= $shipper;
                            if ($orderlist->delivery_staff) {
                                $staff = Sentinel::findById($orderlist->delivery_staff->user_id);
                                $orderlists[$key]['delivery_staff']['user']  = $staff;  
                            }
                            
                        }  
                    $orderlists[$key]['shipping_transaction']= $trans;                    
                 }
            }
            else{
                $orderlists = Delivery_transaction::where('delivery_staff_id',$user->id)->where('status',"pending")->orderBy('created_at', 'desc')->get();   
                foreach ($orderlists as $key => $orderlist) {
                    $trans = Shipping_transaction::with('shipper','delivery_company','cargo_address','shipping_information','shipping_transaction_detail')->where('id',$orderlist->shipping_transaction_id)->get();
                        foreach ($trans as $i => $tran) {
                            foreach ($tran->shipping_transaction_detail as  $j=>$value) {
                                 $cargo = Cargo::where('id',$value->cargo_id)->first();
                                 $trans[$i]['shipping_transaction_detail'][$j]['cargo'] = $cargo;
                            }   
                            $shipper = User::where('id',$tran->shipper->user_id)->first(); 
                            $trans[$i]['shipper']['user']= $shipper;   
                        }  
                    $orderlists[$key]['shipping_transaction']= $trans;  

                 }
            }            return view('admin.orderlist.neworderlist',compact("orderlists"));
            return response()->json($orderlists);
        }

        /**
         * Display the specified resource.
         *
         * For Api 
         * @return \Illuminate\Http\Response
         */
        public function apineworderlist($id)
        {
            $users = Sentinel::findById($id);
            $user = Delivery_staff::where('user_id',$users->id)->first();
            if ($users->inRole('deliverycompany'))
            {
                 $orderlists = Delivery_transaction::where('delivery_company_id',$user->delivery_company_id)->orderBy('created_at', 'desc')->where('delivery_staff_id',0)->get();
                 foreach ($orderlists as $key => $orderlist) {
                    $trans = Shipping_transaction::with('shipper','delivery_company','cargo_address','shipping_information','shipping_transaction_detail')->where('id',$orderlist->shipping_transaction_id)->get();
                        foreach ($trans as $i => $tran) {
                            foreach ($tran->shipping_transaction_detail as  $j=>$value) {
                                 $cargo = Cargo::where('id',$value->cargo_id)->first();
                                 $trans[$i]['shipping_transaction_detail'][$j]['cargo'] = $cargo;
                            }   
                            $shipper = User::where('id',$tran->shipper->user_id)->first(); 
                            $trans[$i]['shipper']['user']= $shipper;         
                        }  
                    $orderlists[$key]['shipping_transaction']= $trans;                    
                 }
            }
            else{
                $orderlists = Delivery_transaction::where('delivery_staff_id',$user->id)->where('status',"pending")->orderBy('created_at', 'desc')->get();   
                foreach ($orderlists as $key => $orderlist) {
                    $trans = Shipping_transaction::with('shipper','delivery_company','cargo_address','shipping_information','shipping_transaction_detail')->where('id',$orderlist->shipping_transaction_id)->get();
                        foreach ($trans as $i => $tran) {
                            foreach ($tran->shipping_transaction_detail as  $j=>$value) {
                                 $cargo = Cargo::where('id',$value->cargo_id)->first();
                                 $trans[$i]['shipping_transaction_detail'][$j]['cargo'] = $cargo;
                            }   
                            $shipper = User::where('id',$tran->shipper->user_id)->first(); 
                            $trans[$i]['shipper']['user']= $shipper;   
                        }  
                    $orderlists[$key]['shipping_transaction']= $trans;  

                 }
            }
            
            return response()->json($orderlists);
        }

        public function getAcceptorderlist()
        {
            $users = Sentinel::getUser();
            $user = Delivery_staff::where('user_id',$users->id)->first();
            if ($users->inRole('deliverycompany'))
            {
                $orderlists = Delivery_transaction::where('delivery_company_id',$user->delivery_company_id)->orderBy('created_at', 'desc')->where('status',"accept")->get();
                foreach ($orderlists as $key => $orderlist) {
                    $trans = Shipping_transaction::with('shipper','delivery_company','cargo_address','shipping_information','shipping_transaction_detail')->where('id',$orderlist->shipping_transaction_id)->get();
                        foreach ($trans as $i => $tran) {
                            foreach ($tran->shipping_transaction_detail as  $j=>$value) {
                                 $cargo = Cargo::where('id',$value->cargo_id)->first();
                                 $trans[$i]['shipping_transaction_detail'][$j]['cargo'] = $cargo;
                            } 
                            $shipper = User::where('id',$tran->shipper->user_id)->first(); 
                            $trans[$i]['shipper']['user']= $shipper;             
                        }  
                    $orderlists[$key]['shipping_transaction']= $trans;  
                    $staff = Delivery_staff::with('user')->where('id',$orderlist->delivery_staff_id)->first(); 
                    $orderlists[$key]['staff']= $staff;                 
                }
            }
            else{
                 $orderlists = Delivery_transaction::where('delivery_company_id',$user->delivery_company_id)->where('status',"accept")->orderBy('created_at', 'desc')->get();
                 foreach ($orderlists as $key => $orderlist) {
                      $trans = Shipping_transaction::with('shipper','delivery_company','cargo_address','shipping_information','shipping_transaction_detail')->where('id',$orderlist->shipping_transaction_id)->get();
                        foreach ($trans as $i => $tran) {
                            foreach ($tran->shipping_transaction_detail as  $j=>$value) {
                                 $cargo = Cargo::where('id',$value->cargo_id)->first();
                                 $trans[$i]['shipping_transaction_detail'][$j]['cargo'] = $cargo;
                            } 
                            $shipper = User::where('id',$tran->shipper->user_id)->first(); 
                            $trans[$i]['shipper']['user']= $shipper;             
                        }  
                    $orderlists[$key]['shipping_transaction']= $trans;  
                    $staff = Delivery_staff::with('user')->where('id',$orderlist->delivery_staff_id)->first(); 
                    $orderlists[$key]['staff']= $staff;                 
                 }
            }
            return view('admin.orderlist.acceptorderlist',compact("orderlists"));
            return response()->json($orderlists);
        }

        public function apiAcceptorderlist(Request $request,$id)
        {
            $users = Sentinel::findById($id);
            $user = Delivery_staff::where('user_id',$users->id)->first();
            if ($users->inRole('deliverycompany'))
            {
                 $orderlists = Delivery_transaction::where('delivery_company_id',$user->delivery_company_id)->orderBy('created_at', 'desc')->where('status',"accept")->get();
                 foreach ($orderlists as $key => $orderlist) {
                    $trans = Shipping_transaction::with('shipper','delivery_company','cargo_address','shipping_information','shipping_transaction_detail')->where('id',$orderlist->shipping_transaction_id)->get();
                        foreach ($trans as $i => $tran) {
                            foreach ($tran->shipping_transaction_detail as  $j=>$value) {
                                 $cargo = Cargo::where('id',$value->cargo_id)->first();
                                 $trans[$i]['shipping_transaction_detail'][$j]['cargo'] = $cargo;
                            }   
                            $shipper = User::where('id',$tran->shipper->user_id)->first(); 
                            $trans[$i]['shipper']['user']= $shipper;         
                        }  
                    $orderlists[$key]['shipping_transaction']= $trans; 
                    $staff = Delivery_staff::with('user')->where('id',$orderlist->delivery_staff_id)->first(); 
                    $orderlists[$key]['staff']= $staff;                   
                 }
            }
            else{
                $orderlists = Delivery_transaction::where('delivery_staff_id',$user->id)->where('status',"accept")->orderBy('created_at', 'desc')->get();   
                foreach ($orderlists as $key => $orderlist) {
                    $trans = Shipping_transaction::with('shipper','delivery_company','cargo_address','shipping_information','shipping_transaction_detail')->where('id',$orderlist->shipping_transaction_id)->get();
                        foreach ($trans as $i => $tran) {
                            foreach ($tran->shipping_transaction_detail as  $j=>$value) {
                                 $cargo = Cargo::where('id',$value->cargo_id)->first();
                                 $trans[$i]['shipping_transaction_detail'][$j]['cargo'] = $cargo;
                            }   
                            $shipper = User::where('id',$tran->shipper->user_id)->first(); 
                            $trans[$i]['shipper']['user']= $shipper;   
                        }  
                    $orderlists[$key]['shipping_transaction']= $trans;  
                    $staff = Delivery_staff::with('user')->where('id',$orderlist->delivery_staff_id)->first(); 
                    $orderlists[$key]['staff']= $staff;
                 }
            }
            
            return response()->json($orderlists);
        }
        /**
         * Display the specified resource.
         *
         * 
         * @return \Illuminate\Http\Response
         */
        public function getReceiveorderlist()
        {
            $users = Sentinel::getUser();
            $user = Delivery_staff::where('user_id',$users->id)->first();
            if ($users->inRole('deliverycompany'))
            {
                $orderlists = Delivery_transaction::where('delivery_company_id',$user->delivery_company_id)->orderBy('created_at', 'desc')->where('status',"receive")->get();
                foreach ($orderlists as $key => $orderlist) {
                    $trans = Shipping_transaction::with('shipper','delivery_company','cargo_address','shipping_information','shipping_transaction_detail')->where('id',$orderlist->shipping_transaction_id)->get();
                        foreach ($trans as $i => $tran) {
                            foreach ($tran->shipping_transaction_detail as  $j=>$value) {
                                 $cargo = Cargo::where('id',$value->cargo_id)->first();
                                 $trans[$i]['shipping_transaction_detail'][$j]['cargo'] = $cargo;
                            } 
                            $shipper = User::where('id',$tran->shipper->user_id)->first(); 
                            $trans[$i]['shipper']['user']= $shipper;             
                        }  
                    $orderlists[$key]['shipping_transaction']= $trans;  
                    $staff = Delivery_staff::with('user')->where('id',$orderlist->delivery_staff_id)->first(); 
                    $orderlists[$key]['staff']= $staff;                 
                }
            }
            else{
                 $orderlists = Delivery_transaction::where('delivery_company_id',$user->delivery_company_id)->where('status',"receive")->orderBy('created_at', 'desc')->get();
                 foreach ($orderlists as $key => $orderlist) {
                      $trans = Shipping_transaction::with('shipper','delivery_company','cargo_address','shipping_information','shipping_transaction_detail')->where('id',$orderlist->shipping_transaction_id)->get();
                        foreach ($trans as $i => $tran) {
                            foreach ($tran->shipping_transaction_detail as  $j=>$value) {
                                 $cargo = Cargo::where('id',$value->cargo_id)->first();
                                 $trans[$i]['shipping_transaction_detail'][$j]['cargo'] = $cargo;
                            } 
                            $shipper = User::where('id',$tran->shipper->user_id)->first(); 
                            $trans[$i]['shipper']['user']= $shipper;             
                        }  
                    $orderlists[$key]['shipping_transaction']= $trans;  
                    $staff = Delivery_staff::with('user')->where('id',$orderlist->delivery_staff_id)->first(); 
                    $orderlists[$key]['staff']= $staff;                 
                 }
            }
            return view('admin.orderlist.receiveorderlist',compact("orderlists"));
            return response()->json($orderlists);
        }

        public function apiReceiveorderlist(Request $request,$id)
        {
            $users = Sentinel::findById($id);
            $user = Delivery_staff::where('user_id',$users->id)->first();
            if ($users->inRole('deliverycompany'))
            {
                 $orderlists = Delivery_transaction::where('delivery_company_id',$user->delivery_company_id)->orderBy('created_at', 'desc')->where('status',"receive")->get();
                 foreach ($orderlists as $key => $orderlist) {
                    $trans = Shipping_transaction::with('shipper','delivery_company','cargo_address','shipping_information','shipping_transaction_detail')->where('id',$orderlist->shipping_transaction_id)->get();
                        foreach ($trans as $i => $tran) {
                            foreach ($tran->shipping_transaction_detail as  $j=>$value) {
                                 $cargo = Cargo::where('id',$value->cargo_id)->first();
                                 $trans[$i]['shipping_transaction_detail'][$j]['cargo'] = $cargo;
                            }   
                            $shipper = User::where('id',$tran->shipper->user_id)->first(); 
                            $trans[$i]['shipper']['user']= $shipper;         
                        }  
                    $orderlists[$key]['shipping_transaction']= $trans;  
                    $staff = Delivery_staff::with('user')->where('id',$orderlist->delivery_staff_id)->first(); 
                    $orderlists[$key]['staff']= $staff;                  
                 }
            }
            else{
                $orderlists = Delivery_transaction::where('delivery_staff_id',$user->id)->where('status',"receive")->orderBy('created_at', 'desc')->get();   
                foreach ($orderlists as $key => $orderlist) {
                    $trans = Shipping_transaction::with('shipper','delivery_company','cargo_address','shipping_information','shipping_transaction_detail')->where('id',$orderlist->shipping_transaction_id)->get();
                        foreach ($trans as $i => $tran) {
                            foreach ($tran->shipping_transaction_detail as  $j=>$value) {
                                 $cargo = Cargo::where('id',$value->cargo_id)->first();
                                 $trans[$i]['shipping_transaction_detail'][$j]['cargo'] = $cargo;
                            }   
                            $shipper = User::where('id',$tran->shipper->user_id)->first(); 
                            $trans[$i]['shipper']['user']= $shipper;   
                        }  
                    $orderlists[$key]['shipping_transaction']= $trans;  
                    $staff = Delivery_staff::with('user')->where('id',$orderlist->delivery_staff_id)->first(); 
                    $orderlists[$key]['staff']= $staff;
                 }
            }
            
            return response()->json($orderlists);
        }

        
        public function getCompleteorderlist()
        {
            $users = Sentinel::getUser();
            $user = Delivery_staff::where('user_id',$users->id)->first();
            if ($users->inRole('deliverycompany'))
            {
                 $orderlists = Delivery_transaction::where('delivery_company_id',$user->delivery_company_id)->orderBy('created_at', 'desc')->where('status',1)->get();
                 foreach ($orderlists as $key => $orderlist) {
                    $trans = Shipping_transaction::with('shipper','delivery_company','cargo_address','shipping_information','shipping_transaction_detail')->where('id',$orderlist->shipping_transaction_id)->get();
                        foreach ($trans as $i => $tran) {
                            foreach ($tran->shipping_transaction_detail as  $j=>$value) {
                                 $cargo = Cargo::where('id',$value->cargo_id)->first();
                                 $trans[$i]['shipping_transaction_detail'][$j]['cargo'] = $cargo;
                            }   
                            $shipper = User::where('id',$tran->shipper->user_id)->first(); 
                            $trans[$i]['shipper']['user']= $shipper;         
                        } 
                    $orderlists[$key]['shipping_transaction']= $trans;  
                    $staff = Delivery_staff::with('user')->where('id',$orderlist->delivery_staff_id)->first(); 
                    $orderlists[$key]['staff']= $staff;   
                    $orderlists[$key]['shipping_transaction']= $trans;                    
                 }
            }
            else{
                $orderlists = Delivery_transaction::where('delivery_staff_id',$user->id)->orderBy('created_at', 'desc')->where('status',1)->get();   
                foreach ($orderlists as $key => $orderlist) {
                    $trans = Shipping_transaction::with('shipper','delivery_company','cargo_address','shipping_information','shipping_transaction_detail')->where('id',$orderlist->shipping_transaction_id)->get();
                        foreach ($trans as $i => $tran) {
                            foreach ($tran->shipping_transaction_detail as  $j=>$value) {
                                 $cargo = Cargo::where('id',$value->cargo_id)->first();
                                 $trans[$i]['shipping_transaction_detail'][$j]['cargo'] = $cargo;
                            }   
                            $shipper = User::where('id',$tran->shipper->user_id)->first(); 
                            $trans[$i]['shipper']['user']= $shipper;   
                        }  
                    $orderlists[$key]['shipping_transaction']= $trans;  

                 }
            }
            return view('admin.orderlist.completeorderlist',compact("orderlists"));
            return response()->json($orderlists);
        }

        public function apicompleteorderlist($id)
        {
            $users = Sentinel::findById($id);
            $user = Delivery_staff::where('user_id',$users->id)->first();

            if ($users->inRole('deliverycompany'))
            {
                 $orderlists = Delivery_transaction::where('delivery_company_id',$user->delivery_company_id)->orderBy('created_at', 'desc')->where('status',1)->get();
                 foreach ($orderlists as $key => $orderlist) {
                    $trans = Shipping_transaction::with('shipper','delivery_company','cargo_address','shipping_information','shipping_transaction_detail')->where('id',$orderlist->shipping_transaction_id)->get();
                        foreach ($trans as $i => $tran) {
                            foreach ($tran->shipping_transaction_detail as  $j=>$value) {
                                 $cargo = Cargo::where('id',$value->cargo_id)->first();
                                 $trans[$i]['shipping_transaction_detail'][$j]['cargo'] = $cargo;
                            }   
                            $shipper = User::where('id',$tran->shipper->user_id)->first(); 
                            $trans[$i]['shipper']['user']= $shipper;         
                        } 
                    $orderlists[$key]['shipping_transaction']= $trans;  
                    $staff = Delivery_staff::with('user')->where('id',$orderlist->delivery_staff_id)->first(); 
                    $orderlists[$key]['staff']= $staff;   
                    $orderlists[$key]['shipping_transaction']= $trans;                    
                 }
            }
            else{
                $orderlists = Delivery_transaction::where('delivery_staff_id',$user->id)->orderBy('created_at', 'desc')->where('status',1)->get();   
                foreach ($orderlists as $key => $orderlist) {
                    $trans = Shipping_transaction::with('shipper','delivery_company','cargo_address','shipping_information','shipping_transaction_detail')->where('id',$orderlist->shipping_transaction_id)->get();
                        foreach ($trans as $i => $tran) {
                            foreach ($tran->shipping_transaction_detail as  $j=>$value) {
                                 $cargo = Cargo::where('id',$value->cargo_id)->first();
                                 $trans[$i]['shipping_transaction_detail'][$j]['cargo'] = $cargo;
                            }   
                            $shipper = User::where('id',$tran->shipper->user_id)->first(); 
                            $trans[$i]['shipper']['user']= $shipper;   
                        }  
                    $orderlists[$key]['shipping_transaction']= $trans;  

                 }
            }

            return response()->json($orderlists);
        }


        public function getAddstaff($id)
        {
            $listid = $id;
            $user = Sentinel::getUser();
            $company = Delivery_staff::where('user_id',$user->id)->first();
             
            $staffes = Delivery_staff::where('delivery_company_id',$company->delivery_company_id)->where('user_id','!=',$user->id)->get();
            foreach ($staffes as $key => $value) {           
                  $staff = User::where('id',$value->user_id)->first();
                  $staffes[$key]['user'] = $staff;
            }
            //return response($staffes);
            return view('admin.orderlist.addstaff',compact('staffes','listid'));
        }

        public function getapiaddstaff($id,$user_id)
        {
            $company = Delivery_transaction::find($id);
            $user = Sentinel::findById($user_id);
            $staffes = Delivery_staff::where('delivery_company_id',$company->delivery_company_id)->where('user_id','!=',$user->id)->get();
            foreach ($staffes as $key => $value) {           
                  $staff = User::where('id',$value->user_id)->first();
                  $staffes[$key]['user'] = $staff;
            }
            return response()->json($staffes);
        }

        public function postAddstaff(Request $request,$id)
        {
            $validator = Validator::make($request->all(), [
            'staffname'           => 'required',
             
            ]);

            if ($validator->fails()) {
                if ($request->route()->getPrefix() == "/admin") {
                    return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
                } 
                if($validator->errors()->has('staffname'))
                    return response()->json($validator->errors()->first('staffname'), 400);                          
            }

            $delivery_transaction = Delivery_transaction::find($id);
            $delivery_transaction->delivery_staff_id = $request->staffname;

            $day  = Carbon::parse($request->pick_up_time)->diffForHumans(Carbon::now());
            $delivery_transaction->pick_up_time  = $day;
            $delivery_transaction->status = "pending";
            $delivery_transaction->update();
            if($request->route()->getPrefix() == "/admin") {
                return redirect()->route('neworderlist');
            } 
            return response()->json("Successful");
            
        }


        public function postAccept($id,Request $request)
        {
            $delivery_transaction = Delivery_transaction::find($id);
            if($delivery_transaction){
            	$delivery_transaction->status = "accept";
            	$delivery_transaction->update();
            	$transaction = Shipping_transaction::find($delivery_transaction->shipping_transaction_id);
            	$shipper = Shipper::find($transaction->shipper_id);
            	$user = Sentinel::findById($shipper->user_id);
                if (strpos($delivery_transaction->pick_up_time, 'day') === FALSE) {
                    $time = "လူၾကီးမင္း၏ ပါဆယ္အား".substr($delivery_transaction->pick_up_time,0,-12)."နာရီအတြင္းလာေရာက္ယူေဆာင္ပါမည္";
                }
                else{
                    $time = "လူၾကီးမင္း၏ ပါဆယ္အား".substr($delivery_transaction->pick_up_time,0,-10)."ရက္အတြင္းလာေရာက္ယူေဆာင္ပါမည္";
                }
                $curl = curl_init("http://shopyface.com/api/v1/sms");
                curl_setopt( $curl, CURLOPT_POST , true);
                curl_setopt( $curl, CURLOPT_POSTFIELDS, array(
                    'mobiles'      => $user->phone,
                    'message'      => $time,
                ));
                curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1);
                $auth = curl_exec( $curl );

                $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                curl_close($curl);
                if($http_status == 200){
                    if($request->route()->getPrefix() == "/admin") {
                        return redirect()->route('acceptorderlist');
                    }
                    else{
                        return response()->json("Successful");
                    }
                }
                else{
                    return response()->json($auth);
                }
                
            }
            
            
        }

        public function postCancel($id,Request $request)
        {
            $delivery_transaction = Delivery_transaction::find($id);
            $delivery_transaction->delivery_staff_id = 0;
            $delivery_transaction->status            = 0;
            $delivery_transaction->pick_up_time      = "";
            $delivery_transaction->update();
            if($request->route()->getPrefix() == "/admin") {
                return redirect()->route('neworderlist');
            } 
            return response()->json("Successful");
        }

        public function postReceive($id,Request $request)
        {
            $delivery_transaction = Delivery_transaction::where('tracking_code',$id)->first();
	    if($delivery_transaction){
	        $delivery_transaction->status = "receive";
        	$delivery_transaction->update();

            	if($request->route()->getPrefix() == "/admin") {
                    return redirect()->route('receiveorderlist');
            	} 
        	return response()->json("Successful");
	    }
	    return response()->json("We could not found your tracking code",400);

        }

        public function postComplete($id,Request $request)
        {
            DB::beginTransaction();
            try {
                $delivery_transaction = Delivery_transaction::find($id);
                $delivery_transaction->status = "1";
                $delivery_transaction->update();

                $shipping_transaction = Shipping_transaction::find($delivery_transaction->shipping_transaction_id);
                $shipping_transaction->status = "2";
                $shipping_transaction->update();
               
                $shipper = Shipper::find($shipping_transaction->shipper_id);
                $user = Sentinel::findById($shipper->user_id);

                $data = array(
                    'user'              => $user
                );

                // Send the activation code through email
                Mail::send('admin.emails.completeorder', $data, function ($m) use ($user) {
		            $m->from(Config::get('mail.from.address'),Config::get('mail.from.name'));
                    $m->to($user->email, $user->name );
                    $m->subject('Delivery Complete');
                });
                $curl = curl_init("http://shopyface.com/api/v1/sms");
                curl_setopt( $curl, CURLOPT_POST , true);
                curl_setopt( $curl, CURLOPT_POSTFIELDS, array(
                    'mobiles'      => $user->phone,
                    'message'      => "လူၾကီးမင္း၏ပါဆယ္အားပို ့ေဆာင္ျပီးပါျပီ",
                ));
                curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1);
                $auth = curl_exec( $curl );

                $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                curl_close($curl);
                if($http_status == 200){
                    DB::commit();
                    if($request->route()->getPrefix() == "/admin") {
                        return redirect()->route('completeorderlist');
                    } 
                    return response()->json("Successful");
                    
                }
                else{
                    return response()->json($auth);
                }
            }catch (Exception $e) {
                DB::rollBack();
            }
        }

        
        public function getShipperaddresslist($id)
        {
            $user = Sentinel::findById($id);
            $shipper = Shipper::where('user_id',$user->id)->first();

            $list = Shipping_information::with('city','township')->where('shipper_id',$shipper->id)->get();
            return response()->json($list);
        }

        public function getCargoaddresslist($id)
        {
            $user = Sentinel::findById($id);
            $shipper = Shipper::where('user_id',$user->id)->first();

            $cargo_address_list = Cargo_address::with('city','township')->where('shipper_id',$shipper->id)->get();
            return response()->json($cargo_address_list);
        }

        public function getCargolist($id)
        {
            $shipper = Shipper::where('user_id',$id)->first();
            $cargo = Cargo::where('shipper_id',$shipper->id)->get();

            return response()->json($cargo);
        }

        public function getEditdelivery($id,Request $request)
        {
            $cities = City::all();
            
            $delivery = Delivery_transaction::find($id);
            $transaction = Shipping_transaction::with('shipping_information')->find($delivery->shipping_transaction_id);
            $city = City::find($transaction->shipping_information->city_id);
            $transaction['shipping_information']['city'] = $city;
            $township = Township::find($transaction->shipping_information->township_id);
            $transaction['shipping_information']['township'] = $township;
            $townships = Township::where('city_id',$transaction->shipping_information->city_id)->get();
            
            if ($request->route()->getPrefix() == "/admin") {
                return view('admin.orderlist.editdelivery',compact('transaction','cities','townships','delivery'));
            }
            return response()->json($transaction);
            
        }

        public function postEditdelivery($id,Request $request)
        {
            $validator = Validator::make($request->all(), [
            'address'            => 'required',
            'weight'             => 'required',
            'price'              => 'required',            
            'remark'             => 'required',
            ]);

            if ($validator->fails()) {
                if ($request->route()->getPrefix() == "/admin") {
                    return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
                }
                if($validator->errors()->has('address'))
                    return response()->json($validator->errors()->first('address'), 400);
                if($validator->errors()->has('weight'))
                    return response()->json($validator->errors()->first('weight'), 400);
                if($validator->errors()->has('price'))
                    return response()->json($validator->errors()->first('price'), 400);             
                if($validator->errors()->has('remark'))
                    return response()->json($validator->errors()->first('remark'), 400);                                     
            }
            DB::beginTransaction();
            try {
                $deliverytransaction = Delivery_transaction::find($id);
                $deliverytransaction->remark = $request->remark;
                $deliverytransaction->update();

                $shippingtransaction = Shipping_transaction::find($deliverytransaction->shipping_transaction_id);
                $shippingtransaction->total_weight  = $request->weight;
                $shippingtransaction->total_cost   = $request->price;
                $shippingtransaction->update();

                $shippinginformation = Shipping_information::find($shippingtransaction->shipping_information_id);
                $shippinginformation->address     = $request->address;
                $shippinginformation->city_id     = $request->city;
                $shippinginformation->township_id = $request->township;
                $shippinginformation->update();

                DB::commit();
                if ($request->route()->getPrefix() == "/admin") {
                    return redirect()->route('neworderlist');          
                }
                return response()->json($shippingtransaction);
            }
            catch (Exception $e) {
                DB::rollBack();
            }
        }

        public function getTracking($code)
        {
            $delivery_transaction = Delivery_transaction::with('shipping_transaction')->where('tracking_code',$code)->first();
            $deliverycities = Delivery_city::where('delivery_company_id',$delivery_transaction->delivery_company_id)->get();
            $township = Shipping_information::find($delivery_transaction->shipping_transaction->shipping_information_id);
           
            
            foreach ($deliverycities as $city) {
                if(in_array($township->township_id, json_decode($city->township_ids))){
                    $delivery_working_day = $city->delivery_working_day;
                }
            }
            $day  = $delivery_transaction->created_at->addDays($delivery_working_day)->diffForHumans(Carbon::now());
            
            $tracking = null;
            if ($delivery_transaction->status == "0") {
                $tracking['status'] = 'Not Sending';
                $tracking['time']   = $day;
                return response()->json($tracking);
            }
            elseif ($delivery_transaction->status == "1") {
                $tracking['status'] = 'Complete';
                $tracking['time']   = $day;
                return response()->json($tracking);
            }
            else{
                $tracking['status'] = 'Receive by Staff';
                $tracking['time']   = $day;
                return response()->json($tracking);
            }
        }

        public function getTrackshipping($code)
        {
            $transaction = Delivery_transaction::where('tracking_code',$code)->first();
            $shipping_transactions = Shipping_transaction::with('shipper','delivery_company','cargo_address','shipping_information','shipping_transaction_detail')->where('id',$transaction->shipping_transaction_id)->get();
            foreach ($shipping_transactions as $i => $shipping_transaction) {
                foreach ($shipping_transaction->shipping_transaction_detail as  $key=>$value) {
                    $cargo = Cargo::where('id',$value->cargo_id)->first();
                    $shipping_transactions[$i]['shipping_transaction_detail'][$key]['cargo'] = $cargo;
                } 
                $shipper = User::where('id',$shipping_transaction->shipper->user_id)->first(); 
                $shipping_transactions[$i]['shipper']['user']= $shipper;  
            }
            
            $staff = Delivery_staff::with('user')->where('id',$transaction->delivery_staff_id)->first(); 
            $transaction['staff']= $staff; 
            $transaction['shipping_transaction']= $shipping_transactions;
            return response()->json($transaction);
        }
    
}
