<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Delivery_city;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CargocalculateController extends Controller
{
    public function index(Request $request)
    {
    	$deliverycity = Delivery_city::where('delivery_company_id',$request->delivery_company_id)
                                     ->where('city_id',$request->city_id)
                                     ->where('township_ids','LIKE', '%'.$request->township_id.'%')
                                     ->first();
        if(!$deliverycity){
            return response()->json('Invalid city and township id!', 400);
        } 
        $quick_price     	= $deliverycity->quick_delivery_price;
         
        $price           	= $deliverycity->price;                        
        $over_price      	= $deliverycity->over_weight_price;
        $fragmented_price	= $deliverycity->fragmented_price; 
        $weight         	= ($request->width+$request->height+$request->length)/5000;
        $actual_weight 		= $request->weight;

        if ($request->document == "true") {
            if($request->quick_delivery == "true") {
                $cost        = $deliverycity->document_price+$quick_price;     
            }
            else{
                $cost        = $deliverycity->document_price;
            }
        }
        else{
	        if($weight >= $actual_weight)
	        {    
	            /*if width+height+length is less than equal basic weigh from delivery_city table*/
	            if($weight <= $deliverycity->basic_weight) 
	            {
	                if($request->quick_delivery == "true"){
	                	
	                    if ($request->fragile == "true") { 
	                        $cost  = $price+$quick_price+$fragmented_price;
	                    }
	                    else{
	                        $cost  = $price+$quick_price;
	                    }                                        
	                }
	                else{
	                    if ($request->fragile == "true") {
	                        $cost  = $price+$fragmented_price;
	                    }
	                    else{
	                        $cost  = $price;
	                    }                                        
	                }                                            
	            }
	            else{
	                $over_weight = $weight-$deliverycity->basic_weight;
	                if($request->quick_delivery == "true") {
	                    if ($request->fragile == "true") {
	                         $cost  = (($price+$quick_price)+($over_price*$over_weight))+$fragmented_price;
	                    }
	                    else{
	                        $cost  = ($price+$quick_price)+($over_price*$over_weight);
	                    }
	                    
	                }
	                else{
	                    if ($request->fragile == "true") {
	                        $cost = ($price+($over_price*$over_weight))+$fragmented_price;
	                    }
	                    else{
	                        $cost  = $price+($over_price*$over_weight);
	                    }
	                    
	                }                                     
	            }                                        
	        }   
	        else{
	            /*if total weight is less than equal basic weigh from delivery_city table*/
	            if($actual_weight <= $deliverycity->basic_weight) 
	            {
	                if($request->quick_delivery == "true") {
	                    if ($request->fragile == "true") {
	                        $cost  = $price+$quick_price+$fragmented_price;
	                    }
	                    else{
	                        $cost = $price+$quick_price;
	                    }
	                    
	                }else{
	                    if ($request->fragile == "true") {
	                          $cost  = $price+$fragmented_price;
	                    }
	                    else{
	                        $cost  = $price;
	                    }
	                }
	                
	            }
	            else{
	                $over_weight = $actual_weight-$deliverycity->basic_weight;   
	                if($request->quick_delivery == "true") {
	                    if ($request->fragile == "true") {
	                        $cost  = (($price+$quick_price)+($over_price*$over_weight))+$fragmented_price;
	                    }
	                    else{
	                        $cost  = ($price+$quick_price)+($over_price*$over_weight);
	                    }
	                }else{
	                    if ($request->fragile == "true") {
	                        $cost  = ($price+($over_price*$over_weight))+$fragmented_price;
	                    }
	                    else{
	                        $cost  = $price+($over_price*$over_weight);
	                    }	                    
	                }	                
	            }
	        }
    	}
        return response()->json($cost);
    }
}
