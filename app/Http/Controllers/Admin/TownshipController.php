<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\City;
use App\Township;
use Validator,ErrorException;
use App\Http\Requests\TownshipCreateRequest;
use App\Http\Controllers\Controller;

class TownshipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->route()->getPrefix() == "/admin") {
             $township = Township::with('city')->orderBy('name', 'asc')->paginate(10);         
             return view('admin.township.index', compact('township'));
        }  
        $city_id = $request->city_id;
        if($city_id){
            $township = Township::with('city')->orderBy('name', 'asc')->where('city_id', $city_id)->get();
        }else{
            $township = Township::with('city')->orderBy('name', 'asc')->get();
        }
        return response()->json($township);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $cities = City::orderBy('name', 'asc')->get();
        if ($request->route()->getPrefix() == "/admin") {             
             return view('admin.township.create', compact('cities'));
        }  
        return response()->json($cities);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required|unique:townships,name',
            'name_mm'           => 'required',
            'city'              => 'required',          
        ]);

        if ($validator->fails()) {
            if ($request->route()->getPrefix() == "/admin") {
                return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            }
            if($validator->errors()->has('name'))
                return response()->json($validator->errors()->first('name'), 400);
            if($validator->errors()->has('name_mm'))
                return response()->json($validator->errors()->first('name_mm'), 400);
            if($validator->errors()->has('city'))
                return response()->json($validator->errors()->first('city'), 400);        
        }

        $township = new Township;
         
        $township->name = $request->name;
        $township->name_mm = $request->name_mm;
        $township->city_id = $request->city;
        if ($request->enable == true) {
            $township->status = 1;
        }
        else{
            $township->status = 0;
        }
        $township->save();

        if ($request->route()->getPrefix() == "/admin") {              
             return redirect()->route('admin.township.index');
        }  
        return response()->json($township);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $townships = Township::with('city')->find($id);
        $cities = City::all();
        if ($request->route()->getPrefix() == "/admin") {              
             return view('admin.township.edit', compact('townships','cities'));
        }  
        return response()->json($townships);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validator = Validator::make($request->all(), [
            'name'              => 'required|unique:townships,name,'.$id,
            'name_mm'           => 'required',
            'city'              => 'required',             
        ]);

        if ($validator->fails()) {
            if ($request->route()->getPrefix() == "/admin") {
                return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            }
            if($validator->errors()->has('name'))
                return response()->json($validator->errors()->first('name'), 400);
            if($validator->errors()->has('name_mm'))
                return response()->json($validator->errors()->first('name_mm'), 400);
            if($validator->errors()->has('city'))
                return response()->json($validator->errors()->first('city'), 400);        
        }

        $townships = Township::find($id);        

        $townships->name = $request->name;
        $townships->name_mm = $request->name_mm;
        $townships->city_id = $request->city;
        if ($request->enable == true) {
            $townships->status = 1;
        }
        else{
            $townships->status = 0;
        }
        $townships->update();
        if ($request->route()->getPrefix() == "/admin") {   
             return redirect()->route('admin.township.index');
        }  
        return response()->json($townships);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $township = Township::find($id);
        $township->delete();
        
    }
}
