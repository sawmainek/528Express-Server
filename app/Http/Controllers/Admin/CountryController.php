<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Country;
use Validator,ErrorException;
use App\Http\Requests\CountryCreateRequest;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        if ($request->route()->getPrefix() == "/admin") {
            $countries = Country::with('city')->paginate(10);
             return view('admin.country.index', compact('countries'));
        }
        $countries = Country::with('city')->get();    
        return response()->json($countries);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->route()->getPrefix() == "/admin") {              
             return view('admin.country.create');
        }
         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required|unique:countries,name',
                          
        ]);

        if ($validator->fails()) {
            if ($request->route()->getPrefix() == "/admin") {
                return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            }
            if($validator->errors()->has('name'))
                return response()->json($validator->errors()->first('name'), 400);                   
        }


        $country = new Country;
         
        $country->name = $request->name;
        $country->save();
        if ($request->route()->getPrefix() == "/admin") {
             return redirect()->route('admin.country.index');
        }
        return response()->json($country);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $country = Country::find($id);
        if ($request->route()->getPrefix() == "/admin") {             
             return view('admin.country.edit', compact('country'));
        }        
        return response()->json($country);
         
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required|unique:countries,name',
                          
        ]);

        if ($validator->fails()) {
            if ($request->route()->getPrefix() == "/admin") {
                return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            }
            if($validator->errors()->has('name'))
                return response()->json($validator->errors()->first('name'), 400);                   
        }

        $country = Country::find($id);
         
        $country->name = $request->name;
        $country->update();
        if ($request->route()->getPrefix() == "/admin") {
             return redirect()->route('admin.country.index');
        }
        return response()->json($country);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         
        $country = Country::find($id);
        $country->delete();
    }
}
