<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Sentinel;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function getIndex()
    {
    	if (!Sentinel::check()) 
        {
            return redirect('admin/login')->withInput()->withErrors(array('message' => 'You must be login!'));
        }
        return view('admin.dashboard');
    }
}
