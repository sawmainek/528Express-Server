<?php

namespace App\Http\Controllers\Admin;

use App\Delivery_transaction;
use Illuminate\Http\Request;
use App\Delivery_location;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator,ErrorException;

class DeliverylocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $location = Delivery_location::with('delivery_transaction')->get();
        if ($request->route()->getPrefix() == "/admin") {
              return view('admin.deliverylocation.index',compact('location'));
        }  
        return response()->json($location);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $deliverytransaction = Delivery_transaction::all();
        if ($request->route()->getPrefix() == "/admin") {
              return view('admin.deliverylocation.create',compact('deliverytransaction'));
        } 
        return response()->json($deliverytransaction);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'delivery_transaction'     => 'required',
            'latitude'                 => 'required',
            'longitude'                => 'required',
                          
        ]);

        if ($validator->fails()) {
            
            if($validator->errors()->has('delivery_transaction'))
                return response()->json($validator->errors()->first('delivery_staff'), 400);
            if($validator->errors()->has('latitude'))
                return response()->json($validator->errors()->first('latitude'), 400);
            if($validator->errors()->has('longitude'))
                return response()->json($validator->errors()->first('longitude'), 400);
                                
        }

        $location = new Delivery_location;

        $location->delivery_transaction_id = $request->delivery_transaction;
        $location->latitude                = $request->latitude;
        $location->longitude               = $request->longitude;

        $location->save();
        if ($request->route()->getPrefix() == "/admin") {
              return view('admin.deliverylocation.index',compact('location'));
        } 
        return response()->json($location);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'delivery_transaction'     => 'required',
            'latitude'                 => 'required',
            'longitude'                => 'required',
                          
        ]);

        if ($validator->fails()) {
            
            if($validator->errors()->has('delivery_transaction'))
                return response()->json($validator->errors()->first('delivery_staff'), 400);
            if($validator->errors()->has('latitude'))
                return response()->json($validator->errors()->first('latitude'), 400);
            if($validator->errors()->has('longitude'))
                return response()->json($validator->errors()->first('longitude'), 400);
                                
        }

        $location =  Delivery_location::find($id);

        $location->delivery_transaction_id = $request->delivery_transaction;
        $location->latitude                = $request->latitude;
        $location->longitude               = $request->longitude;

        $location->update();
        if ($request->route()->getPrefix() == "/admin") {
              return view('admin.deliverylocation.index',compact('location'));
        } 
        return response()->json($location);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deliverylocation = Delivery_location::find($id);
        $deliverylocation->delete();
    }
}
