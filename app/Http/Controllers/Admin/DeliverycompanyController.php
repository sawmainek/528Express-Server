<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\City;
use DB;
use File;
use App\Delivery_city;
use App\Township;
use App\Delivery_company;
use Validator,ErrorException;
use App\Http\Requests\DeliverycompanyCreateRequest;
use App\Http\Controllers\Controller;

class DeliverycompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->route()->getPrefix() == "/admin") {
            $deliverycompany = Delivery_company::with('township','city','deliverycity')->paginate(10);
            foreach ($deliverycompany as $i => $delivery_companies)
            {
                 foreach ($delivery_companies->deliverycity as $j => $value) {
                        
                        $city = City::where('id',$value->city_id)->first();
                        $deliverycompany[$i]['deliverycity'][$j]['delivery_city'] = $city;
                        $township_ids = json_decode($value->township_ids);
                        if(is_array($township_ids))
                        {
                            $townships = Township::wherein('id',$township_ids)->get();
                            $deliverycompany[$i]['deliverycity'][$j]['delivery_township'] = $townships;
                        }                  
                  } 
            }
            return view('admin.deliverycompany.index',compact('deliverycompany'));
        }
        $city_id = $request->city_id;
        $township_id = $request->township_id;
        $save_money  = $request->save_money;
        if ($city_id || $township_id) {
            $deliverycities = Delivery_city::where('city_id',$city_id)->get();
            $delivery_company_id = [];
            foreach ($deliverycities as $city) {
                 if(in_array($township_id, json_decode($city->township_ids))){
                    $delivery_company_id[] = $city->delivery_company_id;
                 }
            }
            if ($save_money == "true") {
                $deliverycompany = Delivery_company::with('city','township')->where('save_money',1)->wherein('id',$delivery_company_id)->get();
            }
            else{
                $deliverycompany = Delivery_company::with('city','township')->wherein('id',$delivery_company_id)->get();
            }
            
            return response()->json( $deliverycompany);
             
        }else{
            if ($save_money == "true") {
                $deliverycompany = Delivery_company::with('township','city','deliverycity')->where('save_money',1)->get();
            }
            else{
                $deliverycompany = Delivery_company::with('township','city','deliverycity')->get();
            }
            
            foreach ($deliverycompany as $i => $delivery_companies)
            {
                 foreach ($delivery_companies->deliverycity as $j => $value) {
                        
                        $city = City::where('id',$value->city_id)->first();
                        $deliverycompany[$i]['deliverycity'][$j]['delivery_city'] = $city;
                        $township_ids = json_decode($value->township_ids);
                        if(is_array($township_ids))
                        {
                            $townships = Township::wherein('id',$township_ids)->get();
                            $deliverycompany[$i]['deliverycity'][$j]['delivery_township'] = $townships;
                        }                  
                  } 
            }
        }

       return response()->json($deliverycompany);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $cities = City::orderBy('name', 'asc')->get();
        $townships= Township::all();
        if ($request->route()->getPrefix() == "/admin") {
              return view('admin.deliverycompany.create',compact('cities','townships'));
        }  
        return response()->json([$city,$township]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
       $validator = Validator::make($request->all(), [
            'name'              => 'required|unique:delivery_companies,name',
            'company_no'        => 'required',
            'email'             => 'required|email',
            'address'           => 'required',
            'city'              => 'required',
            'township'          => 'required',
            'phone'             => 'required|numeric',
        ]);

        if ($validator->fails()) {
            if ($request->route()->getPrefix() == "/admin") {
              return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            } 
            if($validator->errors()->has('name'))
                return response()->json($validator->errors()->first('name'), 400);
            if($validator->errors()->has('company_no'))
                return response()->json($validator->errors()->first('company_no'), 400);
            if($validator->errors()->has('email'))
                return response()->json($validator->errors()->first('email'), 400);
            if($validator->errors()->has('address'))
                return response()->json($validator->errors()->first('address'), 400);
            if($validator->errors()->has('township'))
                return response()->json($validator->errors()->first('township'), 400);
            if($validator->errors()->has('phone'))
                return response()->json($validator->errors()->first('phone'), 400);              
        }

        DB::beginTransaction();
        try 
        {
            $deliverycompany = new Delivery_company ;
            //$city = Township::find($request->township);
            if ($file = $request->file('pic'))
            {
                $fileName        = $file->getClientOriginalName();
                $extension       = $file->getClientOriginalExtension() ?: 'png';
                $folderName      = '/uploads/users/';
                $destinationPath = public_path() . $folderName;
                $safeName        = str_random(10).'.'.$extension;
                $file->move($destinationPath, $safeName);
            }

            $deliverycompany->name        = $request->name;
            $deliverycompany->company_no  = $request->company_no;
            $deliverycompany->email       = $request->email;
            $deliverycompany->address     = $request->address;
            $deliverycompany->photo       = isset($safeName)?$safeName:'';
            $deliverycompany->city_id     = $request->city;
            $deliverycompany->township_id = $request->township;
            $deliverycompany->phone       = $request->phone;
            if ($request->save_money == true) {
                $deliverycompany->save_money = 1;
            }
            else{
                $deliverycompany->save_money = 0;
            }
            $deliverycompany->save();

            
            $nothing = true;

            foreach ($request->delivery_city as $key => $value) {

                if($value > 0){
                    $nothing = false;
                    $delivery_city              = new Delivery_city();
                    $delivery_city->delivery_company_id  = $deliverycompany->id;                     
                    $delivery_city->city_id     = $value;
                    $township = $request["delivery_township$key"];
                    if(json_encode($township))
                        $delivery_city->township_ids = json_encode($township);
                    else
                        $delivery_city->township_ids = "All";
                      
                    $delivery_city->price                 = $request->price[$key];
                    $delivery_city->document_price        = $request->documentprice[$key];
                    $delivery_city->over_weight_price     = $request->overweightprice[$key];
                    $delivery_city->quick_delivery_price  = $request->quickdeliveryprice[$key];
                    $delivery_city->fragmented_price      = $request->fragmentedprice[$key];
                    $delivery_city->basic_weight          = $request->basicweight[$key];
                    $delivery_city->delivery_working_day = $request->delivery_working_time[$key];
                    $delivery_city->radius                = $request->radius[$key];
                    $delivery_city->save();
                     
                }
            }

            if($nothing){
                DB::rollBack();
                return response()->json("Choose any delivery city!", 400);
            }
                     
             DB::commit();


        } 
        catch (Exception $e) 
        {
            DB::rollBack();
        }
       
        
        if ($request->route()->getPrefix() == "/admin") {
              return redirect()->route('admin.deliverycompany.index');
        }  
        return response()->json($deliverycompany);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $cities = City::all();
        
       $deliverycompany = Delivery_company::with('township','city','deliverycity')->find($id);
            $township= Township::where('city_id',$deliverycompany->city_id)->get();
             foreach ($deliverycompany->deliverycity as $j => $value) {
                    
                    $city = City::where('id',$value->city_id)->first();
                    $deliverycompany['deliverycity'][$j]['delivery_city'] = $city;
                    $township_ids = json_decode($value->township_ids);
                    if(is_array($township_ids))
                    {
                        $townships = Township::wherein('id',$township_ids)->get();
                        $deliverycompany['deliverycity'][$j]['delivery_township'] = $townships;
                    }                  
        } 
                  
        if ($request->route()->getPrefix() == "/admin") {
              return view('admin.deliverycompany.edit',compact('deliverycompany','cities','township'));
        }  
       return response()->json($deliverycompany);
    }


    public function editCity(Request $request,$id)
    {
        $deliverycity = Delivery_city::find($id);
        $townships = Township::where('city_id',$deliverycity->city_id)->get();
        $city = City::where('id',$deliverycity->city_id)->first();
        $deliverycity['delivery_city'] = $city;
        $township_ids = json_decode($deliverycity->township_ids);
        if(is_array($township_ids))
        {
            $township = Township::wherein('id',$township_ids)->get();
            $deliverycity['delivery_township'] = $township;
        } 
        //return response()->json($deliverycity);
        return view('admin.deliverycompany.editcity',compact('deliverycity','townships'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'company_no'        => 'required',
            'email'             => 'required|email',
            'address'           => 'required',
            'city'              => 'required',
            'township'          => 'required',
            'phone'             => 'required',
        ]);

        if ($validator->fails()) {
            if ($request->route()->getPrefix() == "/admin") {
              return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            } 
            if($validator->errors()->has('name'))
                return response()->json($validator->errors()->first('name'), 400);
            if($validator->errors()->has('company_no'))
                return response()->json($validator->errors()->first('company_no'), 400);
            if($validator->errors()->has('email'))
                return response()->json($validator->errors()->first('email'), 400);
            if($validator->errors()->has('address'))
                return response()->json($validator->errors()->first('address'), 400);
            if($validator->errors()->has('township'))
                return response()->json($validator->errors()->first('township'), 400);
            if($validator->errors()->has('phone'))
                return response()->json($validator->errors()->first('phone'), 400);              
        }
        DB::beginTransaction();
        try 
        {
            $deliverycompany = Delivery_company::find($id);
            if ($file = $request->file('pic'))
            {

                $fileName        = $file->getClientOriginalName();
                $extension       = $file->getClientOriginalExtension() ?: 'png';
                $folderName      = '/uploads/users/';
                $destinationPath = public_path() . $folderName;
                $safeName        = str_random(10).'.'.$extension;
                $file->move($destinationPath, $safeName);

                //delete old pic if exists
                if(File::exists(public_path() . $folderName.$deliverycompany->photo))
                {
                    File::delete(public_path() . $folderName.$deliverycompany->photo);
                }
                $deliverycompany->photo       = $safeName;
            }
             
             $deliverycompany->name        = $request->name;
             $deliverycompany->company_no  = $request->company_no;
             $deliverycompany->email       = $request->email;
             $deliverycompany->address     = $request->address;
             $deliverycompany->township_id = $request->township;
             $deliverycompany->city_id     = $request->city;            
             $deliverycompany->phone       = $request->phone;
             if ($request->save_money == true) {
                 $deliverycompany->save_money = 1;
             }
             else{
                 $deliverycompany->save_money = 0;
             }
             $deliverycompany->update();

             $nothing = true;

            foreach ($request->delivery_city as $key => $value) {

                if($value > 0){
                    $nothing = false;
                    $delivery_city              = new Delivery_city();
                    $delivery_city->delivery_company_id  = $deliverycompany->id;                     
                    $delivery_city->city_id     = $value;
                    $township = $request["delivery_township$key"];
                    if(json_encode($township))
                        $delivery_city->township_ids = json_encode($township);
                    else
                        $delivery_city->township_ids = "All";
                      
                    $delivery_city->price                 = $request->price[$key];
                    $delivery_city->document_price        = $request->documentprice[$key];
                    $delivery_city->over_weight_price     = $request->overweightprice[$key];
                    $delivery_city->quick_delivery_price  = $request->quickdeliveryprice[$key];
                    $delivery_city->fragmented_price      = $request->fragmentedprice[$key];
                    $delivery_city->basic_weight          = $request->basicweight[$key];
                    $delivery_city->delivery_working_day = $request->delivery_working_time[$key];
                    $delivery_city->radius                = $request->radius[$key];
                    $delivery_city->save();
                     
                }
            }
             DB::commit();
            if ($request->route()->getPrefix() == "/admin") {
                  return redirect()->route('admin.deliverycompany.index');
            }  
            return response()->json($deliverycompany);
        }
        catch (Exception $e) 
        {
            DB::rollBack();
        }

    }


    public function postEditcity(Request $request,$id)
    {

         $deliverycity = Delivery_city::find($id);
         $township = $request["delivery_township"];
            if(json_encode($township))
                $deliverycity->township_ids = json_encode($township);
            else
                $delivery_city->township_ids = "All";
          
         $deliverycity->city_id             = $request->delivery_city;
         $deliverycity->price               = $request->price;
         $deliverycity->basic_weight        = $request->basicweight;
         $deliverycity->over_weight_price   = $request->overweightprice;
         $deliverycity->quick_delivery_price= $request->quickdeliveryprice;
         $deliverycity->fragmented_price    = $request->fragmentedprice;
         $deliverycity->document_price      = $request->documentprice;
         $deliverycity->delivery_working_day=$request->delivery_working_day;
         $deliverycity->radius              = $request->radius;

         $deliverycity->update();
         if ($request->route()->getPrefix() == "/admin") {
              return redirect()->route('admin.deliverycompany.index');
        }  

    }

    public function postDeletecity($id)
    {
         $deletedRows = Delivery_city::find($id);
         $deletedRows->delete();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      
        $deliverycompany = Delivery_company::find($id);
        $deliverycompany->delete();
        $deletedRows = Delivery_city::where('delivery_company_id', $id)->delete();
         
    }
}
