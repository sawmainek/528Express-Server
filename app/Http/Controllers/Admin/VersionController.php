<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class VersionController extends Controller
{
    public function getVersion()
    {
    	return response()->json(1);
    }
}
