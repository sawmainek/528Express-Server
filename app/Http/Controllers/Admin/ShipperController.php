<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Validator,ErrorException;
use App\Shipper;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ShipperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shipper = Shipper::all();
        return response()->json($shipper);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'               => 'required|unique:users,name',
            'email'              => 'required|email|max:255|unique:users,email',
            'password'           => 'required|min:6',            
            'phone'              => 'required|min:8',             
        ]);

        if ($validator->fails()) {
            if($validator->errors()->has('name'))
                return response()->json($validator->errors()->first('name'), 400);
            if($validator->errors()->has('phone'))
                return response()->json($validator->errors()->first('phone'), 400);                      
        }
        DB::beginTransaction();
        try 
        {
            $shipper = new Shipper;
            
            $shipper->name = $request->name;
            $shipper->phone = $request->phone;

            $shipper->save();

        } 
        catch (Exception $e) 
        {
            DB::rollBack();
        }
        DB::commit();
        
        return response()->json($shipper);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
