<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Delivery_staff_location;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator,ErrorException;

class DeliverystafflocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $location = Delivery_staff_location::with('delivery_staff')->get();
        return response()->json($location);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'delivery_staff'           => 'required',
            'latitude'                 => 'required',
            'longitude'                => 'required',
            'address'                  => 'required',
                          
        ]);

        if ($validator->fails()) {
            
            if($validator->errors()->has('delivery_staff'))
                return response()->json($validator->errors()->first('delivery_staff'), 400);
            if($validator->errors()->has('latitude'))
                return response()->json($validator->errors()->first('latitude'), 400);
            if($validator->errors()->has('longitude'))
                return response()->json($validator->errors()->first('longitude'), 400);
            if($validator->errors()->has('address'))
                return response()->json($validator->errors()->first('address'), 400);                   
        }

        $location = new Delivery_staff_location;

        $location->delivery_staff_id = $request->delivery_staff;
        $location->latitude          = $request->latitude;
        $location->longitude         = $request->longitude;
        $location->address           = $request->address;

        $location->save();
        return response()->json($location);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
