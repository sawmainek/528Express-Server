<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Delivery_staff;
use App\Delivery_company;
use App\Http\Requests;
use App\User;
use DB;
use Sentinel;
use Validator,ErrorException;
use App\Http\Controllers\Controller;

class DeliverystaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {       
        if ($user = Sentinel::getUser()){
            if ($user->inRole('deliverycompany')){
                 $staff = Delivery_staff::where('user_id',$user->id)->first();
                 $deliverystaffs = Delivery_staff::with('user')->where('delivery_company_id',$staff->delivery_company_id)->orderBy('created_at', 'desc')->paginate(10);
            }
            else {
                $deliverystaffs = Delivery_staff::with('delivery_company','user')->orderBy('created_at', 'desc')->paginate(10);
            }
        }
            
        if($request->route()->getPrefix() == "/admin") {
             return view('admin.deliverystaff.index', compact('deliverystaffs'));
        }   
        return response()->json($deliverystaffs);
    }

    public function getlist($id)
    {
        $user = Sentinel::findById($id);
        if ($user->inRole('deliverycompany')){
            $staff = Delivery_staff::where('user_id',$user->id)->first();
            $deliverystaffs = Delivery_staff::with('user')->where('delivery_company_id',$staff->delivery_company_id)->orderBy('created_at', 'desc')->get();
        }
        else {
            $deliverystaffs = Delivery_staff::with('delivery_company','user')->orderBy('created_at', 'desc')->get();
        }
        return response()->json($deliverystaffs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        if ($user = Sentinel::getUser()){
            if ($user->inRole('deliverycompany')){
                $staff = Delivery_staff::where('user_id',$user->id)->first();                
                $companies = Delivery_company::find($staff->delivery_company_id);
            }
            else {

                  $companies = Delivery_company::all();
            }
        }
       
        //return response($company);
        return view("admin.deliverystaff.create",compact("companies"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'email'             => 'required|email|unique:users,email',
            'password'          => 'required',
            'phone'             => 'required',
            'company'           => 'required',
                          
        ]);

        if ($validator->fails()) {
            if ($request->route()->getPrefix() == "/admin") {
                return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            } 
            if($validator->errors()->has('name'))
                return response()->json($validator->errors()->first('name'), 400);
            if($validator->errors()->has('email'))
                return response()->json($validator->errors()->first('email'), 400);
            if($validator->errors()->has('password'))
                return response()->json($validator->errors()->first('password'), 400);
            if($validator->errors()->has('phone'))
                return response()->json($validator->errors()->first('phone'), 400);
            if($validator->errors()->has('company'))
                return response()->json($validator->errors()->first('company'), 400);                   
        }
        $credentials = [   
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => $request->password,
            'phone'     => $request->phone,             
        ];
        DB::beginTransaction();
        try {
            $user = Sentinel::registerAndActivate($credentials,true);
            $role = $request->role ? $request->role : "deliverystaff";
             
                $rolename = Sentinel::findRoleByName($role);
                if($rolename)
                    $rolename->users()->attach($user);  

                $staff = new Delivery_staff;
                $staff->user_id = $user->id;
                $staff->delivery_company_id = $request->company;
                $staff->save();
             

            DB::commit();
            if($request->route()->getPrefix() == "/admin") {
                return redirect()->route('admin.deliverystaff.index');
            }  
            return response()->json("Successful!");
           
            
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json("something wrong!");
        }

         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $staff = Delivery_staff::with('delivery_company')->where('id',$id)->first();
        $user = Sentinel::findById($staff->user_id);
        $staff["user"] = $user;
         if($request->route()->getPrefix() == "/admin") {
            return view('admin.deliverystaff.edit',compact("staff"));
        } 
        return response()->json($staff);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'email'             => 'required|email',
            'phone'             => 'required',
            'company'           => 'required',
                          
        ]);

        if ($validator->fails()) {
            if ($request->route()->getPrefix() == "/admin") {
                return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            } 
            if($validator->errors()->has('name'))
                return response()->json($validator->errors()->first('name'), 400);
            if($validator->errors()->has('email'))
                return response()->json($validator->errors()->first('email'), 400);
            if($validator->errors()->has('phone'))
                return response()->json($validator->errors()->first('phone'), 400);
            if($validator->errors()->has('company'))
                return response()->json($validator->errors()->first('company'), 400);                   
        }

        $credentials = [   
            'name'      => $request->name,
            'email'     => $request->email,
            'phone'     => $request->phone,
            'company'   => $request->company             
        ];
        DB::beginTransaction();
        try {
            $staff = Delivery_staff::find($id);
            $staff->delivery_company_id = $request->company;
            $staff->update();

            $user = Sentinel::findById($staff->user_id);
            $user->name     = $request->name;
            $user->email    = $request->email;
            if ($request->password) {
                $user->password = $request->password;
            }
            
            $user->phone    = $request->phone;
            $user->update();

            DB::commit();
            if($request->route()->getPrefix() == "/admin") {
                return redirect()->route('admin.deliverystaff.index');
            }  
            return response()->json("Successful!");
           
            
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
         $staff = Delivery_staff::find($id);
         $staff->delete();
         $user = Sentinel::findById($staff->user_id);
         $user->delete();
         if($request->route()->getPrefix() == "/api") {
            return response()->json("Successful");
        } 
    }
}
