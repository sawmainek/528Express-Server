<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Shipping_transaction;
use App\Delivery_transaction;
use App\Http\Requests;
use DB;
use Mail;
use Redis;
use Config;
use App\User;
use Sentinel;
use App\Delivery_staff;
use Carbon\Carbon;
use App\Shipper;
use App\Delivery_company;
use App\Http\Controllers\Controller;

class ConfirmShippingController extends Controller
{
    public function postConfirmshipping(Request $request,$id)
    {
        DB::beginTransaction();
        try {
            $shipping_transaction = Shipping_transaction::with('shipping_transaction_detail')->where('id',$id)->first();
             
            if ($request->status == true) {
                $shipping_transaction->status = 1; 
            }
            else{
                $shipping_transaction->status = 0;
            }
            
            $shipping_transaction->update();

            $delivery_transaction = new Delivery_transaction;
            $delivery_transaction->shipping_transaction_id = $id;
            $delivery_transaction->delivery_company_id     = $shipping_transaction->delivery_company_id;
            $delivery_transaction->tracking_code           = mt_rand(100000000, 999999999);
            $delivery_transaction->status                  = 0;
            $delivery_transaction->save();
            DB::commit();

            foreach ($shipping_transaction->shipping_transaction_detail as $key => $value) {
                if ($value->quick_delivery == 1) {
                    $user = Delivery_company::find($delivery_transaction->delivery_company_id);
                    $data = array(
                        'user'              => $user,
                    );
                    $emails = array($user->email,'sawmainek90@gmail.com');
                    
                    Mail::send('admin.emails.neworder', $data, function ($m) use ($user,$emails) {
                        $m->from(Config::get('mail.from.address'),Config::get('mail.from.name'));
                        $m->to($emails, $user->name);
                        $m->subject('Receive New Orderlist');
                    });
                }
            }
            
            
            // $mytime = Carbon::now();
            // $shipper = Shipper::where('id',$shipping_transaction->shipper_id)->first();
            // $redis = Redis::connection();
            // $redis->publish('notifications', json_encode([
            //     'message'  => "New delivery",
            //     'name'     => $shipper->name,
            //     'company'  => $shipping_transaction->delivery_company_id,
            //     'time'     => $mytime->diffForHumans(),
            // ]));
            
            return response()->json("Successfully!");
            
        } catch (Exception $e) {
            DB::rollBack();
        }
    	
    }
}
