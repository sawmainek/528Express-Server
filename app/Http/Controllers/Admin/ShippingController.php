<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DB;
use Sentinel;
use App\Shipper;
use App\Cargo;
use App\Shipping_transaction;
use App\Delivery_city;
use App\Shipping_transaction_detail;
use App\Shipping_information;
use Validator,ErrorException;
use App\Cargo_address;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ShippingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shipper_id'             => 'required|exists:shippers,user_id',
            'cargo_address'          => 'required',
            'cargo_township_id'      => 'required',
            'cargo_city_id'          => 'required',
            'shipping_name'          => 'required',
            'shipping_phone'         => 'required',
            'shipping_address'       => 'required',
            'shipping_city_id'       => 'required',
            'shipping_township_id'   => 'required',
            //'delivery_company_id'    => 'required',            
        ]);

        if ($validator->fails()) {
            if($validator->errors()->has('shipper_id'))
                return response()->json('You are not shipper user!', 400);
            if($validator->errors()->has('cargo_address'))
                return response()->json($validator->errors()->first('cargo_address'), 400);
            if($validator->errors()->has('cargo_township_id'))
                return response()->json($validator->errors()->first('cargo_township_id'), 400);
            if($validator->errors()->has('cargo_city_id'))
                return response()->json($validator->errors()->first('cargo_city_id'), 400);
            if($validator->errors()->has('shipping_name'))
                return response()->json($validator->errors()->first('shipping_name'), 400);
            if($validator->errors()->has('shipping_phone'))
                return response()->json($validator->errors()->first('shipping_phone'), 400);
            if($validator->errors()->has('shipping_address'))
                return response()->json($validator->errors()->first('shipping_address'), 400);
            if($validator->errors()->has('shipping_city_id'))
                return response()->json($validator->errors()->first('shipping_city_id'), 400);
            if($validator->errors()->has('shipping_township_id'))
                return response()->json($validator->errors()->first('shipping_township_id'), 400);
            // if($validator->errors()->has('delivery_company_id'))
            //     return response()->json($validator->errors()->first('delivery_company_id'), 400);                 
        }


        DB::beginTransaction();
        try 
        {
            /* Find shipper id from shipper table with login user id  */
            $user_id = $request->shipper_id;
            $shipper = Shipper::where('user_id',$user_id)->first();
            
            $response = null;

            /* Insert Cargo Address to cargo_address table */
            $cargo_address = new Cargo_address;
            $cargo_address->shipper_id = $shipper->id;
            $cargo_address->address = $request->cargo_address;
            $cargo_address->city_id = $request->cargo_city_id;
            $cargo_address->township_id = $request->cargo_township_id;
            $cargo_address->save();

            $response['cargo_address'] = $cargo_address;

            /* Insert Shipping information to Shipping_informations table */
            $shipping_information = new Shipping_information;
            $shipping_information->shipper_id   = $shipper->id;
            $shipping_information->name         = $request->shipping_name;
            $shipping_information->email        = $request->shipping_email;
            $shipping_information->address      = $request->shipping_address;
            $shipping_information->phone        = $request->shipping_phone;
            $shipping_information->city_id      = $request->shipping_city_id;
            $shipping_information->township_id  = $request->shipping_township_id;
            $shipping_information->save();

            $response['shipping_information']  = $shipping_information;

            $shipping_transaction = new Shipping_transaction;
            $shipping_transaction->shipper_id               = $shipper->id;
            $shipping_transaction->shipping_information_id  = $shipping_information->id;
            $shipping_transaction->cargo_address_id         = $cargo_address->id;
            $shipping_transaction->save();


            /*Inserting data to cargo table */
            $input = json_decode($request->cargo);
            if($input){
                $totalcost = "";
                $totalweight = "";  
                        
                foreach ($input as $key => $value) {
                    $cargo = new Cargo;
                    $cargo->shipper_id  = $shipper->id;
                    $cargo->name        = $value->item_name;
                                    
                    $deliverycity = Delivery_city::where('delivery_company_id',$request->delivery_company_id)
                                                 ->where('city_id',$request->shipping_city_id)
                                                 ->where('township_ids','LIKE', '%'.$request->shipping_township_id.'%')
                                                 ->first();
                    if(!$deliverycity){
                        DB::rollBack();
                        return response()->json('Invalid city and township id!', 400);
                    }                                      
                     
                    $quick_price     = $deliverycity->quick_delivery_price;
                                           
                    if ($value->document == "true") {
                        $cargo->quantity    = 0;
                        $cargo->width       = 0;
                        $cargo->height      = 0;
                        $cargo->length      = 0;
                        $cargo->weight      = 0;

                        if ($value->quick_delivery == "true") {
                            $cargo->cost        = $deliverycity->document_price+$quick_price;     
                        }
                        else{
                            $cargo->cost        = $deliverycity->document_price;
                        }

                    }
                    else{
                        
                        $cargo->quantity    = $value->quantity;
                        $cargo->width       = $value->width;
                        $cargo->height      = $value->height;
                        $cargo->length      = $value->length;
                        $price           = $deliverycity->price;                        
                        $over_price      = $deliverycity->over_weight_price;
                        $fragmented_price= $deliverycity->fragmented_price; 
                        $weights         = ($value->width*$value->height*$value->length)/5000;                     
                        $unit            = $value->unit ? $value->unit : "kg";
                         /*if unit is kilogram*/
                        if($unit == "kg") {
                            
                            /*if $weights=width+height+length is greater than equal total weight*/
                            if($weights >= $value->weight)
                            {

                                $weight = round($weights*$value->quantity,1);
                                /*if width+height+length is less than equal basic weigh from delivery_city table*/
                                if($weight <= $deliverycity->basic_weight) 
                                {
                                    if($value->quick_delivery == "true") {
                                        if ($value->fragile == "true") { 
                                            $cargo->cost  = $price+$quick_price+$fragmented_price;
                                        }
                                        else{
                                            $cargo->cost  = $price+$quick_price;
                                        }                                        
                                    }
                                    else{
                                        if ($value->fragile == "true") {
                                            $cargo->cost  = $price+$fragmented_price;
                                        }
                                        else{
                                            $cargo->cost  = $price;
                                        }                                        
                                    }                                            
                                }
                                else{
                                    $over_weight = $weight-$deliverycity->basic_weight;
                                    if($value->quick_delivery == "true") {
                                        if ($value->fragile == "true") {
                                             $cargo->cost  = (($price+$quick_price)+($over_price*$over_weight))+$fragmented_price;
                                        }
                                        else{
                                            $cargo->cost  = ($price+$quick_price)+($over_price*$over_weight);
                                        }
                                        
                                    }
                                    else{
                                        if ($value->fragile == "true") {
                                            $cargo->cost = ($price+($over_price*$over_weight))+$fragmented_price;
                                        }
                                        else{
                                            $cargo->cost  = $price+($over_price*$over_weight);
                                        }
                                        
                                    }                                     
                                }
                                $cargo->weight = $weight;

                                                                      
                            }//end if of $weights >= $value->weight
                            /*if width+height+length is less than equal total weight*/
                            else{
                                $weighttotal = round($value->weight * $value->quantity,1);
                                  /*if total weight is less than equal basic weigh from delivery_city table*/
                                if($weighttotal <= $deliverycity->basic_weight) 
                                {
                                    if($value->quick_delivery == "true") {
                                        if ($value->fragile == "true") {
                                            $cargo->cost  = $price+$quick_price+$fragmented_price;
                                        }
                                        else{
                                            $cargo->cost = $price+$quick_price;
                                        }
                                        
                                    }else{
                                        if ($value->fragile == "true") {
                                              $cargo->cost  = $price+$fragmented_price;
                                        }
                                        else{
                                            $cargo->cost  = $price;
                                        }
                                    }
                                    
                                }
                                else{
                                    $over_weight = $weighttotal-$deliverycity->basic_weight;   
                                    if($value->quick_delivery == "true") {
                                        if ($value->fragile == "true") {
                                            $cargo->cost  = (($price+$quick_price)+($over_price*$over_weight))+$fragmented_price;
                                        }
                                        else{
                                            $cargo->cost  = ($price+$quick_price)+($over_price*$over_weight);
                                        }
                                    }else{
                                        if ($value->fragile == "true") {
                                             $cargo->cost  = ($price+($over_price*$over_weight))+$fragmented_price;
                                        }
                                        else{
                                            $cargo->cost  = $price+($over_price*$over_weight);
                                        }
                                        
                                    }
                                    
                                }
                                $cargo->weight = $weighttotal;  

                            }//end else of $weights >= $value->weight 

                        }// end if of $unit == "kg"

                       /*if unit is lb*/
                        else{ 
                        /* $weight = width+height+length */
                         $kgweights   = round($weights/2.2046,1);
                         $valueweight = round($value->weight/2.2046,1);
                          
                            if($kgweights >= $valueweight)
                            {
                                $kgweight = $kgweights * $value->quantity;
                                if($kgweight <= $deliverycity->basic_weight) 
                                {
                                    if($request->quick_delivery == "true") {
                                        if ($value->fragile == "true") {
                                            $cargo->cost  = $price+$quick_price+$fragmented_price;
                                        }
                                        else{
                                            $cargo->cost  = $price+$quick_price;
                                        }
                                    }else{
                                        if ($value->fragile == "true") {
                                            $cargo->cost  = $price+$fragmented_price;
                                        }
                                        else{
                                            $cargo->cost  = $price;
                                        }
                                    }
                                         
                                }
                                else{
                                    if($request->quick_delivery == "true") {
                                        if ($value->fragile == "true") {
                                            $cargo->cost  = $price+$quick_price+$fragmented_price+$over_price;
                                        }
                                        else{
                                            $cargo->cost  = $price+$quick_price+$over_price;
                                        }
                                        
                                    }else{
                                        if ($value->fragile == "true") {
                                            $cargo->cost  = $price+$fragmented_price+$over_price;
                                        }
                                        else{
                                            $cargo->cost  = $price+$over_price;
                                        }
                                        
                                    }                                    
                                }
                                $cargo->weight = $kgweight;                                                                                  
                            }//end if of $kgweights >= $valueweight 

                            else{                           
                                $weighttotal = $valueweight * $value->quantity;
                                if($weighttotal <= $deliverycity->basic_weight) 
                                {
                                    if($request->quick_delivery == "true") {
                                        if ($value->fragile == "true") {
                                            $cargo->cost  = $price+$quick_price+$fragmented_price;
                                        }
                                        else{
                                            $cargo->cost  = $price+$quick_price;
                                        }
                                        
                                    }else{
                                        if ($value->fragile == "true") {
                                            $cargo->cost  = $price+$fragmented_price;
                                        }
                                        else{
                                            $cargo->cost  = $price;
                                        }
                                        
                                    }                                             
                                }
                                else{
                                    if($request->quick_delivery == "true") {
                                        if ($value->fragile == "true") {
                                            $cargo->cost  = $price+$quick_price+$fragmented_price+$over_price;
                                        }
                                        else{
                                            $cargo->cost  = $price+$quick_price+$over_price;
                                        }
                                        
                                    }else{
                                        if ($value->fragile == "true") {
                                            $cargo->cost  = $price+$fragmented_price+$over_price;
                                        }
                                        else{
                                            $cargo->cost  = $price+$over_price;
                                        }
                                        
                                    }
                                }
                                $cargo->weight = $weighttotal;                                       
                            }//end else of $kgweights >= $valueweight  
                        }//end else of if unit is lb    
                    }
                                                      
                     
                    $cargo->save();
                    
                    $totalweight += $cargo->weight;
                    $totalcost  += $cargo->cost;

                    $response['cargo'][$key] = $cargo;
                     
                    $shippingtransactiondetail = new Shipping_transaction_detail;
                    $shippingtransactiondetail->shipping_transaction_id = $shipping_transaction->id;
                    $shippingtransactiondetail->cargo_id                = $cargo->id;
                    $shippingtransactiondetail->cost                    = $cargo->cost;
                    if ($value->quick_delivery == "true") {
                        $shippingtransactiondetail->quick_delivery   = 1 ;
                    }
                     
                    $shippingtransactiondetail->save();

                    $response['shippingdetail'][$key] = $shippingtransactiondetail;
                }//end foreach of $input as $key => $value
                $shipping_transactions= Shipping_transaction::find($shipping_transaction->id);
                $shipping_transaction->delivery_company_id = $request->delivery_company_id;
                $shipping_transaction->total_cost   = $totalcost;
                $shipping_transaction->total_weight = $totalweight;
                if ($request->take_money == "true") 
                {
                    $shipping_transaction->take_money_amount = $request->amount;
                }
                else{
                    $shipping_transaction->take_money_amount = 0;
                }
                if ($request->receiver_pay == "true") {
                    $shipping_transaction->pay_by = "Pay by Receiver";
                }
                else{
                    $shipping_transaction->pay_by = "Pay by Shipper";
                }
                $shipping_transaction->save();
                $response['shipping_transaction']= $shipping_transaction;
                DB::commit();
                return response()->json($response); 
            }else{
                DB::rollBack();
                return response()->json("Invalid cargo object!", 400);
            }
                  
                
        } catch (Exception $e) {
            DB::rollBack();
        }       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $shipping_transaction = Shipping_transaction::find($id);
        $shipping_transaction->delete();
        $shipping_information = Shipping_information::find($shipping_transaction->shipping_information_id);
        $shipping_information->delete();
        $cargo_address = Cargo_address::find($shipping_transaction->cargo_address_id);
        $cargo_address->delete();
        $shipping_transaction_detail = Shipping_transaction_detail::where('shipping_transaction_id',$shipping_transaction->id)->get();
        foreach ($shipping_transaction_detail as $value) {
            $value->delete();
        }
        foreach ($shipping_transaction_detail as $value) {
            $cargo_id = Cargo::find($value->cargo_id);
            $cargo_id->delete();
        }
        return response()->json('Successful');
    }
}
