<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Shipping_transaction;
use App\Http\Requests;
use App\Cargo_address;
use App\Shipping_transaction_detail;
use App\Shipping_information;
use Sentinel;
use App\Delivery_transaction;
use App\Cargo;
use App\Http\Controllers\Controller;

class ShippingtransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Shipping_transaction::with('shipper','shipping_transaction_detail','delivery_company')->orderBy('created_at', 'desc')->paginate(10);
        foreach ($transactions as $key => $value) {
            $user = Sentinel::findById($value->shipper->user_id);
            $transactions[$key]['user'] =$user;

            $shipping_information = Shipping_information::with('city','township')->where('id',$value->shipping_information_id)->get();
            $transactions[$key]['shipping_information'] = $shipping_information;

            $cargo_address = Cargo_address::with('city','township')->where('id',$value->cargo_address_id)->get();
            $transactions[$key]['cargo_address'] = $cargo_address;

            foreach ($value->shipping_transaction_detail as $i => $v) {
                $cargo = Cargo::where('id',$v->cargo_id)->first();
                $transactions[$key]['shipping_transaction_detail'][$i]['cargo'] =$cargo;
            }
            
        }
//       return response()->json($transactions);


        //return response()->json($transactions);
        return view('admin.shippingtransaction.index',compact('transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $shipping_transaction = Shipping_transaction::find($id);
        $shipping_transaction->delete();
        $delivery_transaction = Delivery_transaction::where('shipping_transaction_id',$id)->first();
        if ($delivery_transaction) {
            $delivery_transaction->delete();
        }
        $shipping_information = Shipping_information::find($shipping_transaction->shipping_information_id);
        $shipping_information->delete();
        $cargo_address = Cargo_address::find($shipping_transaction->cargo_address_id);
        $cargo_address->delete();
        $shipping_transaction_detail = Shipping_transaction_detail::where('shipping_transaction_id',$shipping_transaction->id)->get();
        foreach ($shipping_transaction_detail as $value) {
            $value->delete();
        }
        foreach ($shipping_transaction_detail as $value) {
            $cargo_id = Cargo::find($value->cargo_id);
            $cargo_id->delete();
        }
    }
}
