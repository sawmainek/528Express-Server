<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Validator,ErrorException;
use App\Shipping_information;
use App\Township;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ShippinginformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $information = Shipping_information::with('township')->with('city')->get();
        return response()->json($information);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'                  => 'required',
            'address'               => 'required',
            'township'              => 'required',
            'phone'                 => 'required|min:8',
            'email'                 => 'required|email',
             
        ]);

        if ($validator->fails()) {
            if($validator->errors()->has('name'))
                return response()->json($validator->errors()->first('name'), 400);
            if($validator->errors()->has('address'))
                return response()->json($validator->errors()->first('address'), 400);
            if($validator->errors()->has('township'))
                return response()->json($validator->errors()->first('township'), 400);
            if($validator->errors()->has('phone'))
                return response()->json($validator->errors()->first('phone'), 400);
            if($validator->errors()->has('email'))
                return response()->json($validator->errors()->first('email'), 400);            
        }

        $information = new Shipping_information;
        $city = Township::find($request->township);
         
        $information->name        = $request->name;
        $information->address     = $request->address;
        $information->township_id = $request->township;
        $information->city_id     = $city->city_id;
        $information->phone       = $request->phone;
        $information->email       = $request->email;

        $information->save();

        return response()->json($information);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $information = Shipping_information::find($id);
        return response()->json($information);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'                  => 'required',
            'address'               => 'required',
            'township'              => 'required',
            'phone'                 => 'required|min:8',
            'email'                 => 'required|email',
             
        ]);

        if ($validator->fails()) {
            if($validator->errors()->has('name'))
                return response()->json($validator->errors()->first('name'), 400);
            if($validator->errors()->has('address'))
                return response()->json($validator->errors()->first('address'), 400);
            if($validator->errors()->has('township'))
                return response()->json($validator->errors()->first('township'), 400);
            if($validator->errors()->has('phone'))
                return response()->json($validator->errors()->first('phone'), 400);
            if($validator->errors()->has('email'))
                return response()->json($validator->errors()->first('email'), 400);            
        }

        $information = Shipping_information::find($id);
        $city = Township::find($request->township);

        $information->name        = $request->name;
        $information->address     = $request->address;
        $information->city_id     = $city->city_id;
        $information->township_id = $request->township;
        $information->phone       = $request->phone;
        $information->email       = $request->email;

        $information->update();

        return response()->json($information);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
