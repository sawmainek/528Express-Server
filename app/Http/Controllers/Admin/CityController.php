<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Country;
use App\City;
use Sentinel;
use Illuminate\Http\Response;
use Validator,ErrorException;
use App\Http\Requests\CityCreateRequest;
use App\Http\Controllers\Controller;
 

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        if ($request->route()->getPrefix() == "/admin") {
            $city= City::with('country')->with('township')->orderBy('name', 'asc')->paginate(10);
             return view('admin.city.index', compact('city'));
        }
        $city= City::with('country')->with('township')->orderBy('name', 'asc')->get();    
         return response()->json($city);
         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $countries = Country::orderBy('name', 'asc')->get();
        if ($request->route()->getPrefix() == "/admin") {
            return view('admin.city.create',compact('countries',$countries));
        }        
         return response()->json($countries);
         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'name'              => 'required|unique:cities,name',
            'name_mm'           => 'required',
            'code'              => 'required',
            'country'           => 'required',
             
        ]);

        if ($validator->fails()) {
            if ($request->route()->getPrefix() == "/admin") {
                return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            }
            if($validator->errors()->has('name'))
                return response()->json($validator->errors()->first('name'), 400);
            if($validator->errors()->has('name_mm'))
                return response()->json($validator->errors()->first('name_mm'), 400);
            if($validator->errors()->has('code'))
                return response()->json($validator->errors()->first('code'), 400);
            if($validator->errors()->has('country'))
                return response()->json($validator->errors()->first('country'), 400);            
        }


        $city = new City;
         
        $city->name       = $request->name;
        $city->name_mm    = $request->name_mm;
        $city->code       = $request->code;
        $city->country_id = $request->country;
        if ($request->enable == true) {
            $city->status = 1;
        }
        else{
            $city->status = 0;
        }
        $city->save();
        if ($request->route()->getPrefix() == "/admin") {
             return redirect()->route('admin.city.index');
        }
        return response()->json($city);
         
 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $city = City::with('country')->find($id);
        $countries = Country::all();   
        if ($request->route()->getPrefix() == "/admin") {
             return view('admin.city.edit', compact('city','countries'));
        }        
        return response()->json($city);
         
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required|unique:cities,name,'.$id,
            'name_mm'           => 'required',
            'code'              => 'required',
            'country'           => 'required',
             
        ]);

        if ($validator->fails()) {
            if ($request->route()->getPrefix() == "/admin") {
                return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            }
            if($validator->errors()->has('name'))
                return response()->json($validator->errors()->first('name'), 400);
            if($validator->errors()->has('name_mm'))
                return response()->json($validator->errors()->first('name_mm'), 400);
            if($validator->errors()->has('code'))
                return response()->json($validator->errors()->first('code'), 400);
            if($validator->errors()->has('country'))
                return response()->json($validator->errors()->first('country'), 400);            
        }

        $city = City::find($id);
         
        $city->name = $request->name;
        $city->name_mm = $request->name_mm;
        $city->code = $request->code;
        $city->country_id = $request->country;
        if ($request->enable == true) {
            $city->status = 1;
        }
        else{
            $city->status = 0;
        }  
        $city->update();
        if ($request->route()->getPrefix() == "/admin") {
             return redirect()->route('admin.city.index');
        }
        return response()->json($city);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city = City::find($id);
        $city->delete();
    }
}
