<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Validator,ErrorException;
use App\Http\Requests;
use App\Shipper;
use App\Shipping_information;
use App\Shipping_transaction;
use App\Cargo_address;
use Sentinel;
use App\User;
use App\Shipping_transaction_detail;
use App\Delivery_transaction;
use App\Delivery_company;
use App\Cargo;
use App\Http\Controllers\Controller;

class DeliverytransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Delivery_transaction::orderBy('created_at', 'desc')->paginate(10);
         
        foreach ($transactions as $key => $transaction) {
            $trans = Shipping_transaction::with('shipper','delivery_company','shipping_transaction_detail')->where('id',$transaction->shipping_transaction_id)->get();

            foreach ($trans as $i => $tran) {
                 foreach ($tran->shipping_transaction_detail as  $j=>$value) {
                    $cargo = Cargo::where('id',$value->cargo_id)->first();
                    $trans[$i]['shipping_transaction_detail'][$j]['cargo'] = $cargo;
                 }   
                  $shipper = User::where('id',$tran->shipper->user_id)->first(); 
                  $trans[$i]['shipper']['user']= $shipper;

                  $shipping_information = Shipping_information::with('city','township')->where('id',$tran->shipping_information_id)->get();
                  $trans[$i]['shipping_information']=$shipping_information;

                  $cargo_address = Cargo_address::with('city','township')->where('id',$tran->cargo_address_id)->get();
                  $trans[$i]['cargo_address']=$cargo_address;
            }
            
            $transactions[$key]['shipping_transaction']= $trans;
                                         
        }
       //return response()->json($transactions);
        return view('admin.deliverytransaction.index',compact('transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = Delivery_transaction::find($id);
        $transaction->delete();
        $shipping_transaction = Shipping_transaction::find($transaction->shipping_transaction_id);
        $shipping_transaction->delete();
        $shipping_information = Shipping_information::find($shipping_transaction->shipping_information_id);
        $shipping_information->delete();
        $cargo_address = Cargo_address::find($shipping_transaction->cargo_address_id);
        $cargo_address->delete();
        $shipping_transaction_detail = Shipping_transaction_detail::where('shipping_transaction_id',$shipping_transaction->id)->get();
        foreach ($shipping_transaction_detail as $value) {
            $value->delete();
        }
        foreach ($shipping_transaction_detail as $value) {
            $cargo_id = Cargo::find($value->cargo_id);
            $cargo_id->delete();
        }
    }
}
