<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Sentinel;
use Activation;
use DB;
use URL;
use Reminder;
use App\Role;
use Config;
use Mail;
use App\Delivery_staff;
use Session;
use App\User;
use App\Shipper;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator,ErrorException;
use \Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use \Cartalyst\Sentinel\Checkpoints\ThrottlingException;

class AdminController extends Controller
{
	/**
     * Account sign in.
     *
     * @return View
     */
	public function getLogin()
	{
		// Is the user logged in?
        if (Sentinel::check()) {           
            return redirect('admin/dashboard');
        }

        // Show the page
        return View('admin.login');
	}

	/**
     * Account sign in form processing.
     *
     * @return Redirect
     */
	public function postLogin(Request $request)
	{
        /*Check validation*/
        $validator = Validator::make($request->all(), [            
            'email'              => 'required',
            'password'           => 'required|min:6',                        
        ]);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            if ($request->route()->getPrefix() == "/admin") {
              return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            } 
            if($validator->errors()->has('email'))
                return response()->json($validator->errors()->first('email'), 400);
            if($validator->errors()->has('password'))
                return response()->json($validator->errors()->first('password'), 400);             
                         
        }

        $credentials = [
            'login'    => $request->email,
            'password' => $request->password,
        ];

        try {
            // Try to log the user in           
            if($user = Sentinel::authenticate($credentials))
            {   
                $user = User::with('roles')->whereid($user->id)->first();
                $staff = Delivery_staff::where('user_id',$user->id)->first();

                $user['staff']=$staff;

                return response()->json($user);
            }
            $message = 'Invalid Username or Password';

        } catch (NotActivatedException $e) {
	    $credentials = [
    		'login' => $request->email,
	    ];

	    $user = Sentinel::findByCredentials($credentials);
            return response()->json($user,403);
        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();
            $message = "Too many login attemps.Please try again in {$delay} second(s).";
        }

        
        // Redirect back to login page if prefix is admin 
        if ($request->route()->getPrefix() == "/admin") 
        {
            return redirect()->back()->withErrors(array('message'=>$message));
        }
        // Response json if prefix is api  
        return response()->json($message,400);
	}

    public function postSignin(Request $request)
    {
        /*Check validation*/
        $validator = Validator::make($request->all(), [            
            'email'              => 'required|email',
            'password'           => 'required|min:6',                        
        ]);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            if ($request->route()->getPrefix() == "/admin") {
              return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            } 
            if($validator->errors()->has('email'))
                return response()->json($validator->errors()->first('email'), 400);
            if($validator->errors()->has('password'))
                return response()->json($validator->errors()->first('password'), 400);             
                         
        }

        $credentials = [
            'email'    => $request->email,
            'password' => $request->password,
        ];

        try {
            $user = Sentinel::findByCredentials($credentials);
            if ($user == null) {
                 $message = 'Invalid Username or Password';
                 if ($request->route()->getPrefix() == "/admin") 
                 {
                    return redirect()->back()->withErrors(array('message'=>$message));               
                 }  
                 return response()->json($message);
             }
            if ($user->inRole("shipper")) {
                $message = "You are not allow for Admin Panel!";
                 if ($request->route()->getPrefix() == "/admin") 
                 {
                    return redirect()->back()->withErrors(array('message'=>$message));               
                 }  
                 return response()->json($message);
             }
            // Try to log the user in           
            if(Sentinel::authenticate($credentials))
            {                  
                // Redirect to the dashboard page if prefix is admin
                if ($request->route()->getPrefix() == "/admin") 
                {
                    return redirect('admin/dashboard');                    
                }                
                 // Response json if prefix is api
                return response()->json($user); 
            }
            $message = 'Invalid Username or Password';

        } catch (NotActivatedException $e) {
            $message = "Your account is not activate!";
        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();
            $message = "Too many login attemps.Please try again in {$delay} second(s).";

        }

        
        // Redirect back to login page if prefix is admin 
        if ($request->route()->getPrefix() == "/admin") 
        {
            return redirect()->back()->withErrors(array('message'=>$message));
        }
    }

	/**
     * Account register
     *
     * @return View
     */
    public function getRegister()
    {
    	return view('admin.register');
    }

    /**
     * Account register form processing.
     *
     * @return Redirect
     */
    public function postRegister(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'name'               => 'required|unique:users,name',
            'email'              => 'required|email|max:255|unique:users,email',
            'password'           => 'required|min:6',            
            'phone'              => 'required|numeric|min:8|unique:users,phone',
             

        ]);

        if ($validator->fails()) {
            if($validator->errors()->has('name'))
                return response()->json($validator->errors()->first('name'), 400);
            if($validator->errors()->has('email'))
                return response()->json($validator->errors()->first('email'), 400);
            if($validator->errors()->has('password'))
                return response()->json($validator->errors()->first('password'), 400);             
            if($validator->errors()->has('phone'))
                return response()->json($validator->errors()->first('phone'), 400);
             
                         
        }
        if ($file = $request->file('pic'))
        {
            $fileName        = $file->getClientOriginalName();
            $extension       = $file->getClientOriginalExtension() ?: 'png';
            $folderName      = '/uploads/users/';
            $destinationPath = public_path() . $folderName;
            $safeName        = str_random(10).'.'.$extension;
            $file->move($destinationPath, $safeName);
        }

        $credentials = [   
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => $request->password,
            'phone'     => $request->phone,
            'code'      => mt_rand(100000, 999999),
            'photo'     => isset($safeName)?$safeName:$request->photo,
        ];
        DB::beginTransaction();
        try 
        {
            $user = Sentinel::register($credentials);
            $data = array(
                'user'   => $user,
            );
                     
            Mail::send('admin.emails.confirmcode', $data, function ($m) use ($user) {
                $m->from(Config::get('mail.from.address'),Config::get('mail.from.name'));
                $m->to($user->email, $user->name);
                $m->subject('Your 528express Verification code');
            });
            $curl = curl_init("http://shopyface.com/api/v1/sms");
            curl_setopt( $curl, CURLOPT_POST , true);

            curl_setopt( $curl, CURLOPT_POSTFIELDS, array(
                'mobiles'      => $user->phone,
                'message'      => "Your 528express Verification code is ".$user->code,
            ));
	    curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1);
            $auth = curl_exec( $curl );
            $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);
            if($http_status == 200){
                $role = $request->role ? $request->role : "shipper";
                if($role == "shipper")
                {
                    $rolename = Sentinel::findRoleByName($role);
                    if($rolename)
                        $rolename->users()->attach($user);  

                    $shipper = new Shipper;
                    $shipper->user_id = $user->id;
                    $shipper->name = $request->name;
                    $shipper->save();
                }

                //Sentinel::login($user, false);
            }
            else{
                return response()->json('Sorry Something wrong!',400);
            }

        } 
        catch (Exception $e) 
        {
            DB::rollBack();
        }
        DB::commit();
        

        if ($request->route()->getPrefix() == "/admin") {
            return redirect('admin/dashboard'); 
        }  
        
        return response()->json($user);
    }

    public function getActivate($userId,$activationCode = null)
    {
        if ($activationCode == 0) {
           return response()->json("Sorry Your code is not match!",400);
        }
        else{
            $user = Sentinel::findById($userId);
            $activation = Activation::where('user_id',$user->id)->first();
            if ($activation == null) {
                if ($user->code == $activationCode) {
                    $activation = Activation::create($user);
                    if (Activation::complete($user, $activation->code))
                    {
                        return response()->json($user);
                    }
                    else
                    {
                        return response()->json("Activation not found or not completed",400);
                    }
                }
                else{
                    return response()->json("Sorry Your code is not match!",400);
                }
            }
            else{
                return response()->json("Your acoount is already Activated!",400);
            }
            
        }
               
    }

    public function getResend($phone)
    {

        $user = User::where('phone',$phone)->first();
         
        $activation = Activation::where('user_id',$user->id)->first();
        if ($activation == null) {
            $curl = curl_init("http://shopyface.com/api/v1/sms");
            curl_setopt( $curl, CURLOPT_POST , true);
            curl_setopt( $curl, CURLOPT_POSTFIELDS, array(
                'mobiles'      => $user->phone,
                'message'      => "Your Varification code is ".$user->code,
            ));
            curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1);
            $auth = curl_exec( $curl );

            $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);
            if($http_status == 200){
                return response()->json('Resend your code. Please check your mobile phone!');
            }
            else{
                return response()->json('Sorry Something wrong!',400);
            }
        }
        else{
            return response()->json("Your account is already Activated!!");
        }
    }

    public function getForgotpassword()
    {
        return View('admin.forgot-password');
    }

    public function postForgotpassword(Request $request)
    {
         $validator = Validator::make($request->all(), [            
            'email'              => 'required|email|exists:users,email',
                                    
        ]);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            if ($request->route()->getPrefix() == "/admin") {
              return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
            } 
            if($validator->errors()->has('email'))
                return response()->json($validator->errors()->first('email'), 400);             
                         
        }

        $credentials = [
            'email'    => $request->email,
        ];
        $user = Sentinel::findByCredentials($credentials);
        
        if($user)
        {
            //get reminder for user

            $reminder = Reminder::exists($user) ?: Reminder::create($user);

            // Data to be used on the email view
            $data = array(
                'user'              => $user,
                'forgotPasswordUrl' => URL::route('forgot-password-confirm',[$user->id, $reminder->code]),
            );

            // Send the activation code through email
            Mail::send('admin.emails.forgot-password', $data, function ($m) use ($user) {
                $m->to($user->email, $user->first_name . ' ' . $user->last_name);
                $m->subject('Account Password Recovery');
            });
        }
        else
        {
            // Even though the email was not found, we will pretend
            // we have sent the password reset code through email,
            // this is a security measure against hackers.
        }
        
    }

    public function getLogout()
    {
        // Log the user out
        Sentinel::logout();
        return redirect('admin/login');
         
    }
}
