<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// Route::get('/', function () {
//     return view('welcome');
// });
 

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
//Route::get('socket', 'SocketController@index');
// Route::post('sendmessage', 'SocketController@sendMessage');
// Route::get('writemessage', 'SocketController@writemessage');
Route::group(['prefix' => 'admin','middleware' => ['web']], function () {
	 
 
    Route::get('register',['as' => 'register', 'uses' => 'Admin\AdminController@getRegister']);
    Route::post('register','Admin\AdminController@postRegister');
    Route::get('login',['as' => 'login', 'uses' => 'Admin\AdminController@getLogin']);
    Route::post('login','Admin\AdminController@postSignin');
    Route::get('logout',['as' => 'logout', 'uses' => 'Admin\AdminController@getLogout']);

    Route::get('forgot-password',['as' => 'forgot-password', 'uses' => 'Admin\AdminController@getForgotpassword']);
    Route::post('forgot-password',['as' => 'forgot-password', 'uses' => 'Admin\AdminController@postForgotpassword']);

    Route::get('forgot-password-confirm',['as' => 'forgot-password-confirm', 'uses' => 'Admin\AdminController@getForgotpasswordconfirm']);
     
	//Route::get('',['as' => 'dashboard', 'uses' =>'Admin\HomeController@getIndex']);
	Route::get('/dashboard', ['as' => 'dashboard','uses' => 'JoshController@showHome']);

	Route::group(['middleware' => ['sentinel.admin']], function () {
		
		Route::group(['middleware' => ['role.admin']], function () {
			/* for user */
			Route::resource('user','Admin\UserController');
			Route::resource('role','Admin\RoleController');

			/* for city */
			Route::resource('city','Admin\CityController');

			/* for country */
			Route::resource('country','Admin\CountryController');

			/* for township */
			Route::resource('township','Admin\TownshipController');
		});

		/* for delivery */
		Route::get('deliverycompany/editcity/{id}',['as' => 'deliverycompany/editcity', 'uses' =>'Admin\DeliverycompanyController@editCity']);
		Route::post('deliverycompany/editcity/{id}','Admin\DeliverycompanyController@postEditcity');
		Route::post('deletecity/{id}',['as' => 'deletecity', 'uses' =>'Admin\DeliverycompanyController@postDeletecity']);
		Route::resource('deliverycompany','Admin\DeliverycompanyController');
		Route::resource('deliverystaff','Admin\DeliverystaffController');
		Route::resource('deliverytransaction','Admin\DeliverytransactionController');
		Route::resource('deliverystafflocation','Admin\DeliverystafflocationController');
		Route::resource('deliverylocation','Admin\DeliverylocationController');


		/* for Shipper */
		Route::resource('shipping','Admin\ShippingController');
		// Route::resource('shipper','Admin\ShipperController');
		// Route::resource('shippinginformation','Admin\ShippinginformationController');
		Route::resource('shippingtransaction','Admin\ShippingtransactionController');
		Route::post('confirm-shipping/{id}','Admin\ConfirmShippingController@postConfirmshipping')->middleware('throttle:1');

		/*For Shippging list by Shipper*/
		Route::get('shippinglist/{id}',['as' => 'shippinglist', 'uses' =>'Admin\MylistController@getShippinglist']);
		Route::get('notsendinglist/{id}',['as' => 'notsendinglist', 'uses' =>'Admin\MylistController@getNotsendinglist']);
		Route::get('pendinglist/{id}',['as' => 'pendinglist', 'uses' =>'Admin\MylistController@getPendinglist']);

		Route::group(['middleware' => ['role.staff']], function () {
			/*For Receive Order list by Delivery staff*/
			Route::get('neworderlist',['as' => 'neworderlist', 'uses' =>'Admin\MylistController@getNeworderlist']);
			Route::get('receiveorderlist',['as' => 'receiveorderlist', 'uses' =>'Admin\MylistController@getReceiveorderlist']);
			Route::get('acceptorderlist',['as' => 'acceptorderlist', 'uses' =>'Admin\MylistController@getAcceptorderlist']);
			Route::get('completeorderlist',['as' => 'completeorderlist', 'uses' => 'Admin\MylistController@getCompleteorderlist']);
			Route::get('addstaff/{id}',['as' => 'addstaff', 'uses' =>'Admin\MylistController@getAddstaff']);
			Route::post('addstaff/{id}','Admin\MylistController@postAddstaff');
			Route::post('complete/{id}',['as' => 'complete', 'uses' =>'Admin\MylistController@postComplete']);
			Route::post('accept/{id}',['as' => 'accept', 'uses' =>'Admin\MylistController@postAccept']);
			Route::post('cancel/{id}',['as' => 'cancel', 'uses' =>'Admin\MylistController@postCancel']);
			Route::post('receive/{id}',['as' => 'receive', 'uses' =>'Admin\MylistController@postReceive']);
			Route::get('editdelivery/{id}',['as' => 'editdelivery', 'uses' =>'Admin\MylistController@getEditdelivery']);
			Route::post('editdelivery/{id}',['as' => 'editdelivery', 'uses' =>'Admin\MylistController@postEditdelivery']);
		});
		/* For Parcel */
		Route::resource('parcel','Admin\ParcelController');


	}); 
	
});
 
