<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery_company extends Model
{
    public function city()
    {
    	return $this->belongsTo('App\City');
    }
    public function deliverycity()
    {
    	return $this->hasMany('App\Delivery_city','delivery_company_id');
    }
    public function township()
    {
    	return $this->belongsTo('App\Township');
    }
}
