<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Township extends Model
{
    public function city()
    {
    	return $this->belongsTo('App\City');
    }

    public function delivery_company()
    {
    	return $this->hasMany('App\Delivery_company');
    }

    public function delivery_city()
    {
    	return $this->hasMany('App\Delivery_city');
    }
     
}
