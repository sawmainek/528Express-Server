<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery_staff_location extends Model
{
    public function delivery_staff()
    {
    	return $this->belongsTo('App\Delivery_staff');
    }
}
