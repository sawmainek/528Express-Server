<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo_address extends Model
{
    public function city()
    {
    	return $this->belongsTo('App\City');
    }
    public function township()
    {
    	return $this->belongsTo('App\Township');
    }
}
