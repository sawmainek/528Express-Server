<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery_staff extends Model
{
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function delivery_company()
    {
    	return $this->belongsTo('App\Delivery_company');
    }

    public function delivery_transaction()
    {
    	return $this->hasMany('App\Delivery_transaction');
    }

    public function delivery_staff_location()
    {
        return $this->hasMany('App\Delivery_staff_location');
    }
}
