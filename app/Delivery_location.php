<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery_location extends Model
{
    public function delivery_transaction()
    {
    	return $this->belongsTo('App\Delivery_transaction');
    }
}
