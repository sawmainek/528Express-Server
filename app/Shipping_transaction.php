<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping_transaction extends Model
{
    public function delivery_transaction()
    {
    	return $this->hasMany('App\Delivery_transaction');
    }
    public function shipping_transaction_detail()
    {
    	return $this->hasMany('App\Shipping_transaction_detail');
    }
    public function shipping_information()
    {
    	return $this->belongsTo('App\Shipping_information');
    }
    public function shipper()
    {
        return $this->belongsTo('App\Shipper');
    }
     public function delivery_company()
    {
        return $this->belongsTo('App\Delivery_company');
    }
    public function cargo_address()
    {
        return $this->belongsTo('App\Cargo_address');
    }
}
