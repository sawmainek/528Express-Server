<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery_transaction extends Model
{
    public function delivery_staff()
    {
    	return $this->belongsTo('App\Delivery_staff');
    }
    public function shipping_transaction()
    {
    	return $this->belongsTo('App\Shipping_transaction');
    }
    public function delivery_location()
    {
    	return $this->hasMany('App\Delivery_location');
    }
}
