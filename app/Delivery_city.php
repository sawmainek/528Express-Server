<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery_city extends Model
{
     public function delivery_company()
    {
    	return $this->belongsTo('App\Delivery_company');
    }
    public function city()
    {
    	return $this->belongsTo('App\City');
    }
    public function township()
    {
    	return $this->belongsTo('App\Township');
    }
}
