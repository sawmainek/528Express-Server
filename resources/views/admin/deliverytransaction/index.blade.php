@extends('admin.layout.default')
@section('title')
Delivery Transaction List
@stop
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.colReorder.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.scroller.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/dataTables/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css">
<!--end of page level css-->
@stop
@section('content')
<section class="content-header">
    <h1>Delivery Transaction</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Delivery Transaction</li>
        <li class="active">Delivery Transaction</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left"> <i class="livicon" data-name="list" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Delivery Transaction List
                    </h4>
                </div>
                <div class="panel-body">
                <table class="table table-bordered" id="sample_editable_1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Shipper</th>                             
                            <th>Receiver</th>
                            <th>Cargo</th>
                            <th>Company</th>
                            <th>Status</th>
                            <th>Create Date</th>
                            @if ($user = Sentinel::getUser())    
                                @if ($user->inRole('admin') || $user->inRole('deliverycompany'))
                                    <th>Delete</th>
                                @endif
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($transactions as $key => $transaction)
                        <tr>  
                        <td>{{ $key+1 }}</td>
                            @foreach($transaction->shipping_transaction as $shipping)
                            <td><strong>Shipper Name::</strong>{{ $shipping->shipper->name }}</br>
                                <strong>Shipper Phone::</strong>{{$shipping->shipper->user->phone}}</br>
                                @foreach($shipping->cargo_address as $cargo)
                                <strong>Shipper Address::</strong>{{$cargo->address}}</br>
                                <strong>City::</strong>{{$cargo->city->name}}</br>
                                <strong>Township::</strong>{{$cargo->township->name}}
                                @endforeach
                                
                            </td>
                            <td>
                                @foreach($shipping->shipping_information as $info)
                                <strong>Name::</strong>{{ $info->name }}</br>
                                <strong>Address::</strong>{{ $info->address }}</br>
                                <strong>Phone::</strong> {{ $info->phone }}</br>
                                <strong>City::</strong> {{ $info->city->name}}</br>
                                <strong>Township::</strong> {{$info->township->name}}
                                @endforeach                     
                            </td>
                            <td>
                                @foreach($shipping->shipping_transaction_detail as $detail)
                                    <strong>Name::</strong>{{ $detail->cargo->name }}</br>
                                    <strong>Quantity::</strong>{{ $detail->cargo->quantity }}</br>
                                    <strong>Weight::</strong>{{ $detail->cargo->weight }}</br>
                                    <strong>Cost::</strong>{{ $detail->cargo->cost }}    
                                @endforeach
                            </td>
                            <td>
                                <strong>Name::</strong>{{$shipping->delivery_company->name }} </br>
                                <strong>Phone::</strong>{{$shipping->delivery_company->phone }}
                            </td>
                            @endforeach 
                        <td>
                            @if($transaction->status == "0")
                            <div class="alert-danger" role="alert">{{"Not Sending"}}</div>                            
                            @elseif($transaction->status == "1")
                            <div class="alert-success" role="alert">{{"complete"}}</div>                     
                            @else
                            <div class="alert-info" role="alert">{{$transaction->status}}</div>     
                            @endif
                        </td>
                        <td>{{ $transaction->created_at->diffForHumans()}}</td>
                        @if ($user = Sentinel::getUser())    
                            @if ($user->inRole('admin') || $user->inRole('deliverycompany'))
                                <td>
                                    <div class="col-xs-1">                          
                                         <a href="javascript:deleteUser('{{ $transaction->id }}');">
                                            <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete"></i>
                                         </a>
                                    </div>     
                                </td>
                            @endif
                        @endif
                        </tr>
                        
                        @endforeach 
                    </tbody>
                </table>
                {!! $transactions->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>   
@stop
@section('footer_scripts')
 <!-- begining of page level js -->
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.tableTools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/dataTables/dataTables.bootstrap.js') }}"></script>
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    </div>
  </div>
</div>
<script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>

<script type="text/javascript">
    $(function(){
        var table = $('#sample_editable_1').DataTable({
              "bInfo": false,
              "bPaginate": false,
               
          } );
        
    });

    function deleteUser(id) {
        if (confirm('Are you sure want to delete?')) {
        $.ajax({
            type: "DELETE",
            url: '/admin/deliverytransaction/' + id, //resource
            success: function() {
                    window.location = 'deliverytransaction';
                 
            }
        });
    }
}
</script>
@stop
 