@extends ('admin.layout.default')
@section('header_styles')
<!--page level css -->
    <link rel="stylesheet" type="text/css" href="/assets/css/select2css/select2.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/select2css/select2.min.css">
	<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
	<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<!--end of page level css-->
@stop
@section('content')
<section class="content-header">
    <h1>Delivery Company</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Delivery Company</li>
        <li class="active">Delivery Company</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="users-add" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Edit Delivery Company
                    </h4>
                </div>
                <div class="panel-body">
                	<div class="alert-danger">
						 {{ $errors->first('message') }}
					</div>
					<div class="col-md-6 col-md-offset-3">
						<form class="form-horizontal" action="{{ route('admin.deliverycompany.update', $deliverycompany->id) }}" method="post">
							<input name="_method" type="hidden" value="PUT">
							 
							 <div id="delivery_city_container">
							 	
							 </div>
							 <p>
							 	<button class="btn btn-primary" type="button" id="add_delivery_city">Add Next City</button>
							 </p>
							<button type="submit" class="btn btn-primary">Update</button>
							<a class="btn btn-primary" href="{{ route('admin.deliverycompany.index') }}">Cancel</a>
						</form>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop 
@section('footer_scripts')		
	<link rel="stylesheet" type="text/css" href="/assets/css/select2css/select2.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/select2css/select2.min.css">			
    <script type="text/javascript" src="/assets/js/select2js/select2.js"></script>
    <script type="text/javascript" src="/assets/js/select2js/select2.min.js"></script>
    
	<script type="text/javascript" >
	$("#buttoncity").on('change', function(){
	            var state_id = $(this).val();
	            if(state_id > 0){
	                $.ajax({
	                    url:'/api/township',
	                    method: 'get',
	                    data: {
	                        'city_id': state_id
	                    },
	                    success: function (response){
	                         console.log(response);
	                        $("#township").html('');
	                        $.each(response, function(key, value) {
	                            $("#township").append($("<option></option>").val(value.id).html(value.name));
	                        });
		                    },
		                    error: function (response) {
		                        alert(response.status + ' ' + response.statusText);
		                    }
	                });
	            }else{
	                 $("#township").html('<option value="default">&ndash; Select an option &ndash;</option>');
	            	 
	            }
	         });
	 	var index = 0;
		var addDeliveryCity = function(){

			var makeid = function(){
					    var text = "";
					    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

					    for( var i=0; i < 5; i++ )
					        text += possible.charAt(Math.floor(Math.random() * possible.length));

					    return text;
					};

			var ram_id = makeid();
			var template = 		'<p>'+
								' 	<label for="delivery_city" class="control-label">Delivery City</label>'+
								' 	<select class="js-example-placeholder-single delivery_city" name="delivery_city[]" style="width:100%;">'+
								'	    <option value="default">&ndash; Select a city &ndash;</option>'+
								'	    @foreach ($cities as $city)  '+
								'	  		<option value={{ $city->id }}>{{ $city->name }}</option>}'+
								'	  	@endforeach	   			'+			   
								'	</select>	'+
								' </p>'+
								' <p>'+
								' 	<label for="delivery_township" class="control-label">Delivery Township</label>	'+
								' 	<select id="'+ram_id+'" class="js-example-placeholder-multiple" name="delivery_township'+(index++)+'[]" multiple="multiple" style="width:100%;">	'+			    
								'	   	<option value="default">&ndash; Select a township &ndash;</option>   		'+			   						 
								'	</select>'+
								' </p>'+
								' <div class="col-xs-6">'+
								' 	<label for="price" class="control-label">Price</label>	'+
								'   <input type="text" name="price[]" class="form-control" />'+
								' </div>'+
								' <div class="col-xs-6">'+
								' 	<label for="basicweight" class="control-label">Basic Weight</label>	'+
								'   <input type="text" name="basicweight[]" class="form-control" />'+
								' </div>'+
								' <div class="col-xs-6">'+
								' 	<label for="overweightprice" class="control-label">Over Weight Price</label>	'+
								'   <input type="text" name="overweightprice[]" class="form-control" />'+
								' </div>'+
								' <div class="col-xs-6">'+
								' 	<label for="quickdeliveryprice" class="control-label">Quick Delivery Price</label>	'+
								'   <input type="text" name="quickdeliveryprice[]" class="form-control" />'+
								' </div>'+
								' <div class="col-xs-6">'+
								' 	<label for="fragmentedprice" class="control-label">Fragmented Price</label>	'+
								'   <input type="text" name="fragmentedprice[]" class="form-control" />'+
								' </div>'+
								' <div class="col-xs-6">'+
								' 	<label for="documentprice" class="control-label">Document Price</label>	'+
								'   <input type="text" name="documentprice[]" class="form-control" />'+
								' </div>'+
								'<p> '+
								' 	<label for="radius" class="control-label">Radius</label>'+
								'   <input type="text" name="radius[]" class="form-control" />'+
								'</p>';

			$('#delivery_city_container').append(template);

			$(".js-example-placeholder-single").select2({
			  placeholder: "Select a city",
			  allowClear: true
			});
			 
			$(".js-example-placeholder-multiple").select2({
			   placeholder: "Select a township"
			});

			
			$(".delivery_city").on('change', function(){
	            var state_id = $(this).val();
	            if(state_id > 0){
	                $.ajax({
	                    url:'/api/township',
	                    method: 'get',
	                    data: {
	                        'city_id': state_id
	                    },
	                    success: function (response){
	                        console.log(response);
	                        $("#"+ram_id).html('');
	                        $.each(response, function(key, value) {
	                            $("#"+ram_id).append($("<option></option>").val(value.id).html(value.name));
	                        });
	                        $(".js-example-placeholder-multiple").select2({
							   placeholder: "Select a township"
							});
	                    },
	                    error: function (response) {
	                        alert(response.status + ' ' + response.statusText);
	                    }
	                });
	            }else{
	                $("#"+ram_id).html('<option value="default">&ndash; Select an option &ndash;</option>');
	            	$(".js-example-placeholder-multiple").select2({
					   placeholder: "Select a township"
					});
	            }

	        });
		};

		$(".js-example-placeholder-single").select2({
			  placeholder: "Select a city",
			  allowClear: true
			});
			 
			$(".js-example-placeholder-multiple").select2({
			   placeholder: "Select a township"
			});

		$('#add_delivery_city').click(function(){
			addDeliveryCity();
		});

		
    	 
    	  
	</script>
@stop