@extends ('admin.layout.default')
@section('title')
Edit Delivery City
@stop
@section('header_styles')
<!--page level css -->
    <link rel="stylesheet" type="text/css" href="/assets/css/select2css/select2.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/select2css/select2.min.css">
	<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
	<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<!--end of page level css-->
@stop
@section('content')
<section class="content-header">
    <h1>Delivery Company</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Delivery Company</li>
        <li class="active">Delivery Company</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="users-add" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Edit Delivery Company
                    </h4>
                </div>
                <div class="panel-body">
                	<div class="alert-danger">
						 {{ $errors->first('message') }}
					</div>
					<div class="col-md-6 col-md-offset-3">
						<form class="form-horizontal" action="{{ route('deliverycompany/editcity', $deliverycity->id) }}" method="post">
							<p>
							 	<label for="delivery_city" class="control-label">Delivery City</label>
							 	<select class="js-example-placeholder-single delivery_city" name="delivery_city" style="width:100%;">
								  	<option value="{{ $deliverycity->delivery_city->id }}" >{{ $deliverycity->delivery_city->name }}</option>  				   
								</select>
							</p>
							<p>
							 	<label for="delivery_township" class="control-label">Delivery Township</label>	
							 	<select class="js-example-placeholder-multiple" name="delivery_township[]" multiple="multiple" style="width:100%;">		    
								    @foreach($townships as $township)
								      	{{ $exist = false}} 
								      	@foreach($deliverycity->delivery_township as $value)
								      	 	@if($value->name  == $township->name)
								      	 		{{$exist = true}}
								      	 	@endif
								      	@endforeach
							      	 	@if($exist)	
							      	 		<option value="{{ $township->id }}" selected>{{ $township->name }}</option>
							      	 	@else
							      	 		<option value="{{ $township->id }}">{{ $township->name }}</option>
							      	 	@endif
								    @endforeach	 		   						 
								</select>
							</p>
						 	<div class="col-xs-6">
							 <label for="price" class="control-label">Price</label>	 
							   <input type="text" name="price" class="form-control" value="{{$deliverycity->price}}" /> 
							</div>
							<div class="col-xs-6">
							 	<label for="basicweight" class="control-label">Basic Weight</label>	 
							    <input type="text" name="basicweight" class="form-control" value="{{$deliverycity->basic_weight}}" /> 
							</div>
							<div class="col-xs-6">
								 <label for="overweightprice" class="control-label">Over Weight Price</label> 
								 <input type="text" name="overweightprice" class="form-control" value="{{$deliverycity->over_weight_price}}"/> 
							</div> 
							<div class="col-xs-6"> 
							 	<label for="quickdeliveryprice" class="control-label">Quick Delivery Price</label> 
						   		<input type="text" name="quickdeliveryprice" class="form-control" value="{{$deliverycity->quick_delivery_price}}"/> 
							</div> 
							<div class="col-xs-6"> 
							 	<label for="fragmentedprice" class="control-label">Fragmented Price</label> 
							    <input type="text" name="fragmentedprice" class="form-control" value="{{$deliverycity->fragmented_price}}"/> 
							</div> 
							<div class="col-xs-6">
							 	<label for="documentprice" class="control-label">Document Price</label>	 
							    <input type="text" name="documentprice" class="form-control" value="{{$deliverycity->document_price}}"/> 
							</div>
							<div class="col-xs-6">
							 	<label for="delivery_working_day" class="control-label">Delivery Working Day</label>	 
							    <input type="text" name="delivery_working_day" class="form-control" value="{{$deliverycity->delivery_working_day}}"/> 
							</div>
							<div class="col-xs-6"> 
								<label for="radius" class="control-label">Radius</label>
								<input type="text" name="radius" class="form-control" value="{{$deliverycity->radius}}"/>
							</div>
							<div class="col-xs-12">
								<button type="submit" class="btn btn-primary">Update</button>
								<a class="btn btn-primary" href="{{ route('admin.deliverycompany.edit',$deliverycity->delivery_company_id) }}">Cancel</a>
							</div>	
						</form>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop 
@section('footer_scripts')		
	<link rel="stylesheet" type="text/css" href="/assets/css/select2css/select2.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/select2css/select2.min.css">			
    <script type="text/javascript" src="/assets/js/select2js/select2.js"></script>
    <script type="text/javascript" src="/assets/js/select2js/select2.min.js"></script>
    <script type="text/javascript">
    	$(".js-example-placeholder-single").select2({
			  placeholder: "Select a city",
			   
			});
			 
			$(".js-example-placeholder-multiple").select2({
			   placeholder: "Select a township"
			});
    </script>
@stop