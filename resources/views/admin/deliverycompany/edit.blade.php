@extends ('admin.layout.default')
@section('title')
Edit Delivery Company
@stop
@section('header_styles')
<!--page level css -->
   <link rel="stylesheet" type="text/css" href="/assets/css/select2css/select2.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/select2css/select2.min.css">
	<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/wizard.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendors/wizard/jquery-steps/css/jquery.steps.css') }}">
	<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" />
<!--end of page level css-->
@stop
@section('content')
<section class="content-header">
    <h1>Delivery Company</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Delivery Company</li>
        <li class="active">Delivery Company</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="users-add" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Edit Delivery Company
                    </h4>
                </div>
                <div class="panel-body">
                	<div class="alert-danger">
						 {{ $errors->first('message') }}
					</div>
					<div class="col-md-6 col-md-offset-3">
						<form class="form-horizontal" action="{{ route('admin.deliverycompany.update', $deliverycompany->id) }}" method="post" enctype="multipart/form-data">
							<input name="_method" type="hidden" value="PUT">
							 
							<div class="form-group">
                                <label for="pic" class="col-sm-2 control-label">Profile picture</label>
                                <div class="col-sm-10">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
                                            @if($deliverycompany->photo)
                                                <img src="{!! url('/').'/uploads/users/'.$deliverycompany->photo !!}" alt="profile pic">
                                            @else
                                                <img src="http://placehold.it/200x200" alt="profile pic">
                                            @endif
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                        <div>
                                            <span class="btn btn-default btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input id="pic" name="pic" type="file" class="form-control" />
                                            </span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                             </div>
						     <p>
								<label for="name" class="control-label">Company Name</label>
								<input type="text" name="name" class="form-control" value="{{ $deliverycompany->name }}" />
								 <div class="errors">
									{{ $errors->first('name') }}	
								</div>
							 </p>

							 <p>
								<label for="company_no" class="control-label">Company No</label>
								<input type="text" name="company_no" class="form-control" value="{{ $deliverycompany->company_no }}" />
								<div class="errors">
									{{ $errors->first('company_no') }}
								</div>
								 
							 </p>
							 <p>
								<label for="email" class="control-label">Email</label>
								<input type="text" name="email" class="form-control" value="{{ $deliverycompany->email }}" />
								<div class="errors">
									{{ $errors->first('email') }}
								</div>
								 
							 </p>
							 <p>
								<label for="address" class="control-label">Address</label>
								<input type="text" name="address" class="form-control" value="{{ $deliverycompany->address }}" />
								<div class="errors">
									{{ $errors->first('address') }}
								</div>
								 
							 </p>
							 <p>
								<label for="city" class="control-label">City</label>								 
								<select id="buttoncity" class="form-control" name="city">
								   	@foreach ($cities as $roles)  
								  		<option value="{{ $roles->id }}" {{ $roles->name  == $deliverycompany->city->name ? 'selected' : null }}>{{ $roles->name }}</option>}
								  	@endforeach						   
								</select>								 			
							 </p>
							 <p>
								<label for="township" class="control-label">Township</label>								 
								<select id="township" class="form-control" name="township">
								   	@foreach ($township as $roles) 
								  		<option value="{{ $roles->id }}" {{ $roles->name  == $deliverycompany->township->name ? 'selected' : null }}>{{ $roles->name }}</option>}
								  	@endforeach					   					   
								</select>								 			
							 </p>
							 <p>
								<label for="phone" class="control-label">Phone Number</label>
								<input type="text" name="phone" class="form-control" value="{{ $deliverycompany->phone }}" />
								<div class="errors">
									{{ $errors->first('phone') }}	
								</div>
							 </p>
							 <div id="delivery_city_container">
							 	
							 </div>
							 <div class="col-xs-12">
							 	<input name="save_money" type="checkbox" id="chkquick" {{ $deliverycompany->save_money  == 1 ? 'checked' : null }}
                                       onchange="if(this.checked) this.value='true'; else this.value='false';">
							 	<label for="save_money">Save Money</label>
							 </div>
							 <div class="col-xs-12">
							 	<button class="btn btn-primary" type="button" id="add_delivery_city">Add Next City</button>
							 </div>
							 <div class="col-xs-12">
								<button type="submit" class="btn btn-primary">Update</button>
								<a class="btn btn-primary" href="{{ route('admin.deliverycompany.index') }}">Cancel</a>
							</div>
						</form>
					</div>
					<table class="table table-bordered" >
	                    <thead>
	                        <tr>
	                            <th>Delivery City</th>
	                            <th>Delivery Township</th>
	                            <th>Price</th>
	                            <th>Document Price</th>
	                            <th>OverWeight Price</th>
	                            <th>Quick Delivery Price</th>
	                            <th>Fragmented Price</th>
	                            <th>Basic Weight</th>
	                            <th>Edit/Delete</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	@foreach($deliverycompany->deliverycity as $value)
	                         <tr>	                         
	                         	<td>{{$value->delivery_city->name}}</td>
	                         	<td>
	                         		@foreach($value->delivery_township as $township)
	                         			{{$township->name}},
	                         		@endforeach
	                         	</td>
	                         	<td>{{$value->price}}</td>
	                         	<td>{{$value->document_price}}</td>
	                         	<td>{{$value->over_weight_price}}</td>
	                         	<td>{{$value->quick_delivery_price}}</td>
	                         	<td>{{$value->fragmented_price}}</td>
	                         	<td>{{$value->basic_weight}}</td>
	                         	<td>
	                         		<div class="col-xs-1"> 
		                                <a href="{{ route('deliverycompany/editcity', $value->id) }}">
		                                    <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit"></i>
		                                </a>
		                            </div>
		                            <div class="col-xs-1">                              
		                                <a href="javascript:deleteUser('{{ $value->id }}');">
                                    		<i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete"></i>
                                 		</a>
		                            </div>    
	                         	</td>
	                         </tr>
	                        @endforeach
	                    </tbody>
	                </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop 
@section('footer_scripts')		
	<script type="text/javascript" src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/wizard/jquery-steps/js/jquery.steps.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/pages/form_wizard.js') }}"></script>		
    <script type="text/javascript" src="/assets/js/select2js/select2.js"></script>
    <script type="text/javascript" src="/assets/js/select2js/select2.min.js"></script>			
    
    
	<script type="text/javascript">
		$("#buttoncity").on('change', function(){
		            var state_id = $(this).val();
		            if(state_id > 0){
		                $.ajax({
		                    url:'/api/township',
		                    method: 'get',
		                    data: {
		                        'city_id': state_id
		                    },
		                    success: function (response){
		                         console.log(response);
		                        $("#township").html('');
		                        $.each(response, function(key, value) {
		                            $("#township").append($("<option></option>").val(value.id).html(value.name));
		                        });
			                    },
			                    error: function (response) {
			                        alert(response.status + ' ' + response.statusText);
			                    }
		                });
		            }else{
		                 $("#township").html('<option value="default">&ndash; Select an option &ndash;</option>');
		            	 
		            }
		         });

		function deleteUser(id) {
	        if (confirm('Are you sure want to delete?')) {
	            $.ajax({
	                type: "post",
	                url: '/admin/deletecity/' + id, //resource
	                success: function() {
	                	 
	                    window.location = '';
	                }
	            });
	        }
	    }

	    var index = 0;
		 	var index1 = 0;
			var addDeliveryCity = function(){

				var makeid = function(){
						    var text = "";
						    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

						    for( var i=0; i < 5; i++ )
						        text += possible.charAt(Math.floor(Math.random() * possible.length));

						    return text;
						};

				var ram_id = makeid();
				var template = 		'<p>'+
									' 	<label for="delivery_city" class="control-label">Delivery City</label>'+
									' 	<select class="js-example-placeholder-single delivery_city" name="delivery_city[]" style="width:100%;">'+
									'	    <option value="default">&ndash; Select a city &ndash;</option>'+
									'	    @foreach ($cities as $city)  '+
									'	  		<option value={{ $city->id }}>{{ $city->name }}</option>}'+
									'	  	@endforeach	   			'+			   
									'	</select>	'+
									' </p>'+
									' <p>'+
									' 	<label for="delivery_township" class="control-label">Delivery Township</label>	'+
									' 	<select id="'+ram_id+'" class="js-example-placeholder-multiple" name="delivery_township'+(index++)+'[]" multiple="multiple" style="width:100%;">	'+			    
									'	   	<option value="default">&ndash; Select a township &ndash;</option>   		'+			   						 
									'	</select>'+
									' </p>'+
									' <div class="col-xs-6">'+
									' 	<label for="price" class="control-label">Price</label>	'+
									'   <input type="text" name="price[]" class="form-control" />'+
									' </div>'+
									' <div class="col-xs-6">'+
									' 	<label for="basicweight" class="control-label">Basic Weight</label>	'+
									'   <input type="text" name="basicweight[]" class="form-control" />'+
									' </div>'+
									' <div class="col-xs-6">'+
									' 	<label for="overweightprice" class="control-label">Over Weight Price</label>	'+
									'   <input type="text" name="overweightprice[]" class="form-control" />'+
									' </div>'+
									' <div class="col-xs-6">'+
									' 	<label for="quickdeliveryprice" class="control-label">Quick Delivery Price</label>	'+
									'   <input type="text" name="quickdeliveryprice[]" class="form-control" />'+
									' </div>'+
									' <div class="col-xs-6">'+
									' 	<label for="fragmentedprice" class="control-label">Fragmented Price</label>	'+
									'   <input type="text" name="fragmentedprice[]" class="form-control" />'+
									' </div>'+
									' <div class="col-xs-6">'+
									' 	<label for="documentprice" class="control-label">Document Price</label>	'+
									'   <input type="text" name="documentprice[]" class="form-control" />'+
									' </div>'+
									'<div class="col-xs-6">'+
									' 	<label for="delivery_working_time" class="control-label">Delivery Working Day</label>	'+
									'   <input type="text" name="delivery_working_time[]" class="form-control" />'+
									' </div>'+
									'<div class="col-xs-6"> '+
									' 	<label for="radius" class="control-label">Radius</label>'+
									'   <input type="text" name="radius[]" class="form-control" />'+
									'</div>';

				$('#delivery_city_container').append(template);

				$(".js-example-placeholder-single").select2({
				  placeholder: "Select a city",
				  allowClear: true
				});
				 
				$(".js-example-placeholder-multiple").select2({
				   placeholder: "Select a township"
				});

				
				$(".delivery_city").on('change', function(){
		            var state_id = $(this).val();
		            if(state_id > 0){
		                $.ajax({
		                    url:'/api/township',
		                    method: 'get',
		                    data: {
		                        'city_id': state_id
		                    },
		                    success: function (response){
		                        console.log(response);
		                        $("#"+ram_id).html('');
		                        $.each(response, function(key, value) {
		                            $("#"+ram_id).append($("<option></option>").val(value.id).html(value.name));
		                        });
		                        $(".js-example-placeholder-multiple").select2({
								   placeholder: "Select a township"
								});
		                    },
		                    error: function (response) {
		                        alert(response.status + ' ' + response.statusText);
		                    }
		                });
		            }else{
		                $("#"+ram_id).html('<option value="default">&ndash; Select an option &ndash;</option>');
		            	$(".js-example-placeholder-multiple").select2({
						   placeholder: "Select a township"
						});
		            }

		        });
			};

			addDeliveryCity();

			$('#add_delivery_city').click(function(){
				addDeliveryCity();
			});
	 	 
	</script>
@stop