@extends ('admin.layout.default')
@section('title')
Edit Township
@stop
@section('content')
<section class="content-header">
    <h1>Township</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Township</li>
        <li class="active">Edit Township</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="users-add" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Edit Township
                    </h4>
                </div>
                <div class="panel-body">
                	<div class="alert-danger">
						 {{ $errors->first('message') }}
					</div>
					<div class="col-md-6 col-md-offset-3">
						<form class="form-horizontal" action="{{ route('admin.township.update',$townships->id ) }}" method="post">
						<input name="_method" type="hidden" value="PUT">
						<?php echo csrf_field(); ?>
							 <p>
								<label for="name" class="control-label">Township Name</label>
								<input type="text" name="name" class="form-control" value="{{ $townships->name }}" />
								<div class="errors">
									{{ $errors->first('name') }}	
								</div>			
							 </p>
							 <p>
								<label for="name_mm" class="control-label">Township Myanmar Name</label>
								<input type="text" name="name_mm" class="form-control" value="{{ $townships->name_mm }}" />
								<div class="errors">
									{{ $errors->first('name_mm') }}	
								</div>		
							 </p>	
							 <p>
								<label for="city" class="control-label">City</label>								 
								<select class="form-control" name="city">
								   	<?php foreach ($cities as $city) { ?>
								  		<option value="{{ $city->id }}" {{ $city->name  == $townships->city->name ? 'selected' : null }}>{{ $city->name }}</option>}
								  	<?php } ?>						   
								</select>								 			
							 </p>
							 <p>
							 	<input name="enable" type="checkbox" id="chkquick" {{ $townships->status  == 1 ? 'checked' : null }}
                                       onchange="if(this.checked) this.value='true'; else this.value='false';">
							 	<label for="enable">Enable</label>
							 </p>
							<button type="submit" class="btn btn-primary">Update</button>
							<a class="btn btn-primary" href="{{ route('admin.township.index') }}">Cancel</a>
						</form>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop