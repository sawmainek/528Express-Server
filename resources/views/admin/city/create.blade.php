@extends ('admin.layout.default')
@section('title')
Create City
@stop
@section('content')
<section class="content-header">
    <h1>City</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>City</li>
        <li class="active">Create City</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="archive-add" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Create City
                    </h4>
                </div>
                <div class="panel-body">
                	<div class="alert-danger">
						 {{ $errors->first('message') }}
					</div>
					<div class="col-md-6 col-md-offset-3">
						<form class="form-horizontal" action="{{ route('admin.city.store') }}" method="post">
							 <p>
								<label for="name" class="control-label">City Name</label>
								<input type="text" name="name" class="form-control"/>
								<div class="errors">
									{{ $errors->first('name') }}	
								</div>
								 			
							 </p>
							 <p>
								<label for="name_mm" class="control-label">City Myanmar Name</label>
								<input type="text" name="name_mm" class="form-control"/>
								<div class="errors">
									{{ $errors->first('name_mm') }}	
								</div>	 			
							 </p>
							 <p>
								<label for="code" class="control-label">City Code</label>
								<input type="text" name="code" class="form-control"/>
								 <div class="errors">
									{{ $errors->first('code') }}	
								</div>				
							 </p>
							 <p>
								<label for="country" class="control-label">Country</label>								 
								<select class="form-control" name="country">
								   
								   	<?php foreach ($countries as $roles) { ?>
								  		<option value={{ $roles->id }}><?php echo $roles->name ; ?></option>}
								  	<?php } ?>						   
								</select>								 			
							 </p>
							 <p>
							 	<input name="enable" type="checkbox" id="chkquick" value="false"
                                       onchange="if(this.checked) this.value='true'; else this.value='false';">
							 	<label for="enable">Enable</label>
							 </p>
							<button type="submit" class="btn btn-primary">Create</button>
							<a class="btn btn-primary" href="{{ route('admin.city.index') }}">Cancel</a>
						</form>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>    
	 
@stop