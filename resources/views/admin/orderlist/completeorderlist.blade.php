@extends('admin.layout.default')
@section('title')
Complete Order List
@stop
@section('header_styles')
<!--page level css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/js/dataTables/dataTables.bootstrap.css') }}">
<!--end of page level css-->
@stop
@section('content')
<section class="content-header">
    <h1>Complete Order List</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Complete Order List</li>
        <li class="active">Complete Order List</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left"> <i class="livicon" data-name="list" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Complete Order List
                    </h4>
                </div>
                <div class="panel-body">
                	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                            	<th>No</th>
                                <th>Shipper</th>                             
                                <th>Receiver</th>
                                <th>Cargo</th>
                                <th>Total Weight & Cost</th>
                                @if ($user = Sentinel::getUser())    
                                    @if ($user->inRole('deliverycompany'))
                                    <th>Staff</th>
                                    @endif
                                @endif
                                <th>Create Date</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orderlists as $key => $orderlist)
                            <tr>  
                            <td>{{ $key+1 }}</td>
                            @foreach($orderlist->shipping_transaction as $list)
                            <td><strong>Shipper Name::</strong>{{ $list->shipper->name }}</br>
                            	<strong>Shipper Address::</strong>{{ $list->cargo_address->address }}</br>
                            	<strong>Shipper Phone::</strong>{{$list->shipper->user->phone}}
                            </td>
                            <td><strong>Name::</strong>{{ $list->shipping_information->name }}</br>
                            	<strong>Address::</strong>{{ $list->shipping_information->address }}</br>
                            	<strong>Phone::</strong>{{ $list->shipping_information->phone }}                       	
                            </td>
                            <td>
                                @foreach($list->shipping_transaction_detail as $value)
                                <strong>Name::</strong>{{ $value->cargo->name }}</br>
                            	<strong>Quantity::</strong>{{ $value->cargo->quantity }}</br>
                            	<strong>Weight::</strong>{{ $value->cargo->weight}}</br>
                            	<strong>Cost::</strong>{{ $value->cargo->cost}}
                                @endforeach
                            </td>
                            <td>
                                <strong>Weight::</strong>{{$list->total_weight}}</br>
                                <strong>Cost::</strong>{{$list->total_cost}}
                            </td>
                            @endforeach
                            @if ($user = Sentinel::getUser())    
                                @if ($user->inRole('deliverycompany'))
                                    <td>
					@if($orderlist->staff == null)
					   {{"No Staff"}}
					@else
				      	{{ $orderlist->staff->user->name}}
					@endif
				    </td>
                                @endif
                            @endif
                            <td>{{ $orderlist->created_at->diffForHumans()}}</td>
                             <td>
                                 @if($orderlist->status == 1)
                                    {{"Complete"}}
                                 @endif
                             </td>
                            </tr>
                            @endforeach 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('footer_scripts')
 <script src="{{ asset('assets/js/dataTables/jquery.dataTables.js') }}"></script>
    <script src="{{asset('assets/js/dataTables/dataTables.bootstrap.js') }}"></script>
    <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
@stop
