@extends('admin.layout.default')
@section('title')
New Order List
@stop
@section('header_styles')
<!--page level css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/js/dataTables/dataTables.bootstrap.css') }}">
<!--end of page level css-->
@stop
@section('content')
<section class="content-header">
    <h1>New Order List</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>New Order List</li>
        <li class="active">New Order List</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left"> <i class="livicon" data-name="list" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        New Order List
                    </h4>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Shipper</th>                             
                                <th>Receiver</th>
                                <th>Cargo</th>
                                <th>Total Weight & Cost</th>
                                <th>Create Date</th>
                                <th>Edit</th>
                                @if ($user = Sentinel::getUser())    
                                    @if ($user->inRole('deliverycompany'))
                                    <th>Choose Staff</th>
                                    @else
                                    <th>Accept</th>
                                    @endif
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orderlists as $key => $orderlist)
                            <tr>  
                            <td>{{ $key+1 }}</td>
                            @foreach($orderlist->shipping_transaction as $list)
                            <td><strong>Shipper Name::</strong>{{ $list->shipper->name }}</br>
                                <strong>Shipper Address::</strong>{{ $list->cargo_address->address }}</br>
                                <strong>Shipper Phone::</strong>{{$list->shipper->user->phone}} 
                            </td>
                            <td><strong>Name::</strong>{{ $list->shipping_information->name }}</br>
                                <strong>Address::</strong>{{ $list->shipping_information->address }}</br>
                                <strong>Phone::</strong>{{ $list->shipping_information->phone }}                        
                            </td>
                            <td>
                                @foreach($list->shipping_transaction_detail as $value)
                                    <strong>Name::</strong>{{$value->cargo->name }}</br>
                                    <strong>Quantity::</strong>{{$value->cargo->quantity }}</br>
                                    <strong>Weight::</strong>{{$value->cargo->weight}}</br>
                                    <strong>Cost::</strong>{{$value->cargo->cost}}              
                                @endforeach
                            </td>
                            <td>
                                <strong>Weight::</strong>{{$list->total_weight}}</br>
                                <strong>Cost::</strong>{{$list->total_cost}}  
                            </td>
                            @endforeach
                            <td>{{ $orderlist->created_at->diffForHumans()}}</td>
                            <td>
                                <a href="{{ route('editdelivery', $orderlist->id) }}">
                                    <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit product"></i>
                                </a>
                            </td>
                             @if ($user = Sentinel::getUser())    
                                @if ($user->inRole('deliverycompany'))
                                    @if ($orderlist->delivery_staff_id == 0)
                                        <td><a class="btn btn-primary" href="{{ route('addstaff',$orderlist->id) }}">Choose Staff</a></td>
                                    @else
                                        <td>{{ $orderlist->delivery_staff->user->name}}</td>
                                    @endif
                                @else
                                    <td>
                                        <form  method="POST" action="{{ route('accept', $orderlist->id) }}" accept-charset="UTF-8">
                                            <button type="submit" class="btn btn-primary">Accept</button>
                                        </form>
                                        <!-- <form  method="POST" action="{{ route('cancel', $orderlist->id) }}" accept-charset="UTF-8" style="margin-top: 3px;"> -->
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target=".bs-example-modal-sm" style="margin-top: 3px;">Cancel</button>
                                            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                                              <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Why are you cancel for this delivery ?</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    <div id="error" class="alert-danger" style="margin-bottom: 3px;">
                                                        
                                                    </div>
                                                    <label for="remark" class="control-label">Reason</label>                                
                                                     <input id="remark" type="text" name="remark" class="form-control" value=""/> 
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <a href="javascript:deleteUser('{{ $orderlist->id }}');" class="btn btn-primary">Ok</a>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        <!-- </form>   -->
                                    </td>
                                @endif
                            @endif
                            </tr>
                            @endforeach 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('footer_scripts')
 <script src="{{ asset('assets/js/dataTables/jquery.dataTables.js') }}"></script>
    <script src="{{asset('assets/js/dataTables/dataTables.bootstrap.js') }}"></script>
    <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });

            function deleteUser(id) {  
                if ($('#remark').val() == "") {
                     $('#error').html("The reason filed is required");
                }
                else{
                    $.ajax({
                        type: "post",
                        url: '/admin/cancel/' + id , 
                        data: {remark: $('#remark').val()},
                        success: function() {
                                window.location = 'neworderlist';
                             
                        }
                    });
                }                
                    
            }
    </script>
@stop
 
