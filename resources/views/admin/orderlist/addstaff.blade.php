@extends('admin.layout.default')
@section('title')
Add Staff
@stop
@section('content')
<section class="content-header">
    <h1>Choose Staff</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Choose Staff</li>
        <li class="active">Choose Staff</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="users-add" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                       Choose Staff
                    </h4>
                </div>
                <div class="panel-body">
                	<div class="alert-danger">
						{{ $errors->first('errors') }}
					</div>
					<div class="col-md-6 col-md-offset-3">
					<form class="form-horizontal" action="{{ route('addstaff',$listid) }}" method="post">
								
						<p>
							<label for="staffname" class="control-label">Staff Name</label>
							<select class="form-control" name="staffname">
								<option value="">Select a Staff</option>								   
								@foreach ($staffes as $staff)
									@if(isset($staff->user->name)) 
									<option value={{ $staff->id }}>{{$staff->user->name}}</option>
									@endif
								@endforeach				   
							</select>			 
							<div class="errors">
								{{ $errors->first('staffname') }}	
							</div>
						</p>
						<p>
                          
                            <div class='input-group date' id='dtp'>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                <input type='text' name="pick_up_time" class="form-control" />
                            </div>
                        
                        </p>
						<button type="submit" class="btn btn-primary">Add</button>
						<a class="btn btn-primary" href="{{ route('neworderlist') }}">Cancel</a>
					</form>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('footer_scripts')
<script type="text/javascript">
    $(function () {
        $('#dtp').datetimepicker();
    });
</script>
@stop
