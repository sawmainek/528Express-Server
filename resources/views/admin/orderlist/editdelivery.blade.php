@extends ('admin.layout.default')

@section('content')
<section class="content-header">
    <h1>Delivery</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Delivery</li>
        <li class="active">Edit Delivery</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="users-add" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Edit Delivery
                    </h4>
                </div>
                <div class="panel-body">
                	<div class="alert-danger">
						 {{ $errors->first('message') }}
					</div>
					<div class="col-md-6 col-md-offset-3">
						<form class="form-horizontal" action="{{ route('editdelivery',$delivery->id ) }}" method="post">
						 
						<?php echo csrf_field(); ?>
							<p>
								<label for="address" class="control-label">Address</label>
								<input type="text" name="address" class="form-control" value="{{ $transaction->shipping_information->address }}" />
								<div class="errors">
									{{ $errors->first('address') }}	
								</div> 			
							</p>
							<p>
								<label for="city" class="control-label">City</label>								 
								<select id="buttoncity" class="form-control" name="city">
								   	@foreach ($cities as $city)  
								  		<option value="{{ $city->id }}" {{ $city->name  == $transaction->shipping_information->city->name  ? 'selected' : null }}>{{ $city->name }}</option>}
								  	@endforeach						   
								</select>								 			
							</p>

							<p>
								<label for="township" class="control-label">Township</label>								 
								<select id="township" class="form-control" name="township">
								   	@foreach ($townships as $township) 
								  		<option value="{{ $township->id }}" {{ $township->name  == $transaction->shipping_information->township->name ? 'selected' : null }}>{{ $township->name }}</option>}
								  	@endforeach					   					   
								</select>								 			
							</p>
							<p>
								<label for="weight" class="control-label">Weight</label>
								<input type="text" name="weight" class="form-control" value="{{ $transaction->total_weight }}" />
								<div class="errors">
									{{ $errors->first('weight') }}	
								</div> 			
							</p>
							<p>
								<label for="price" class="control-label">Price</label>
								<input type="text" name="price" class="form-control" value="{{ $transaction->total_cost }}" />
								<div class="errors">
									{{ $errors->first('price') }}	
								</div> 			
							</p>
							<p>
								<label for="remark" class="control-label">Remark</label>
								<textarea type="text" name="remark" class="form-control" value="">{{$delivery->remark}}</textarea>			
								<div class="errors">
									{{ $errors->first('remark') }}	
								</div> 	
							</p>
							<button type="submit" class="btn btn-primary">Update</button>
							<a class="btn btn-primary" href="{{ route('neworderlist') }}">Cancel</a>
						</form>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('footer_scripts')		
<script type="text/javascript" >
	$("#buttoncity").on('change', function(){
	            var state_id = $(this).val();
	            if(state_id > 0){
	                $.ajax({
	                    url:'/api/township',
	                    method: 'get',
	                    data: {
	                        'city_id': state_id
	                    },
	                    success: function (response){
	                         console.log(response);
	                        $("#township").html('');
	                        $.each(response, function(key, value) {
	                            $("#township").append($("<option></option>").val(value.id).html(value.name));
	                        });
		                    },
		                    error: function (response) {
		                        alert(response.status + ' ' + response.statusText);
		                    }
	                });
	            }else{
	                 $("#township").html('<option value="default">&ndash; Select an option &ndash;</option>');	            	 
	            }
	         });
	 	 
	</script>
@stop