@extends ('admin.layout.default')
@section('title')
Create Delivery Staff
@stop
@section('content')
<section class="content-header">
    <h1>Delivery Staff</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Delivery Staff</li>
        <li class="active">Create Delivery Staff</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="users-add" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Create Delivery Staff
                    </h4>
                </div>
                <div class="panel-body">
	                <div class="alert-danger">
							 {{ $errors->first('message') }}
					</div>
					<div class="col-md-6 col-md-offset-3">
						<form class="form-horizontal" action="{{ route('admin.deliverystaff.store') }}" method="post">						 
						 	<p>
								<label for="name" class="control-label">name</label>
								<input type="text" name="name" class="form-control"/>
								<div class="errors">
									{{ $errors->first('name') }}	
								</div>
								 			
							 </p>
							 <p>
								<label for="email" class="control-label">Email</label>
								<input type="text" name="email" class="form-control"/>
								<div class="errors">
									{{ $errors->first('email') }}	
								</div>
								 			
							 </p>
							 <p>
								<label for="password" class="control-label">Password</label>
								<input type="text" name="password" class="form-control"/>
								 <div class="errors">
									{{ $errors->first('password') }}	
								</div>				
							 </p>
							 <p>
								<label for="phone" class="control-label">Phone</label>
								<input type="text" name="phone" class="form-control"/>
								<div class="errors">
									{{ $errors->first('phone') }}	
								</div> 			
							 </p>
							 <p>
								<label for="company" class="control-label">Company</label>								 
								<select class="form-control" name="company">
								@if($user = Sentinel::getUser())
									@if($user->inRole('deliverycompany'))
									<option value={{ $companies->id }}>{{$companies->name}}</option>
									@else
									<option value="">Select Company</option>
									@foreach($companies as $company)	 
								  		<option value={{ $company->id }}>{{$company->name}}</option>
								   	@endforeach	
									@endif
								@endif	   
								</select>
								<div class="errors">
									{{ $errors->first('company') }}	
								</div> 								 			
							 </p>
							 
							<button type="submit" class="btn btn-primary">Create</button>
							<a class="btn btn-primary" href="{{ route('admin.deliverystaff.index') }}">Cancel</a>
						</form>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>
	 
@stop