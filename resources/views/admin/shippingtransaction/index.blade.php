@extends('admin.layout.default')
@section('title')
Shipping Transaction List
@stop
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.colReorder.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.scroller.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/dataTables/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css">
<!--end of page level css-->
@stop
@section('content')
<section class="content-header">
    <h1>Shipping Transaction</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Shipping Transaction</li>
        <li class="active">Shipping Transaction</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left"> <i class="livicon" data-name="list" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Shipping Transaction List
                    </h4>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered" id="sample_editable_1">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Shipper</th>                             
                                <th style="width: 161px;">Receiver</th>
                                <th>Cargo</th>
                                <th>Company</th>
                                <th>Total Weight & Cost</th>
                                <th>status</th>
                                <th>Date</th>
                                @if ($user = Sentinel::getUser())    
                                    @if ($user->inRole('admin') || $user->inRole('deliverycompany'))
                                        <th>Delete</th>
                                    @endif
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                             @foreach($transactions as $key => $transaction)
                             <tr>
                                 <td>{{ $key+1 }}</td>
                                 <td><strong>Name::</strong>{{$transaction->shipper->name}}</br>
                                     <strong>Phone::</strong>{{$transaction->user->phone}}</br>
                                     @foreach($transaction->cargo_address as $cargo)
                                     <strong>Address::</strong>{{$cargo->address}}</br>
                                     <strong>City::</strong>{{$cargo->city->name}}</br>
                                     <strong>Township::</strong>{{$cargo->township->name}}
                                     @endforeach
                                 </td>
                                 <td>
                                    @foreach($transaction->shipping_information as $information)
                                    <strong>Name::</strong>{{$information->name}}</br>
                                    <strong>Address::</strong>{{$information->address}}</br>
                                    <strong>Phone::</strong>{{$information->phone}}</br>
                                    <strong>City::</strong>{{$information->city->name}}</br>
                                    <strong>Township::</strong>{{$information->township->name}}
                                    @endforeach
                                 </td>
                                 <td>
                                 @foreach($transaction->shipping_transaction_detail as $value)
                                    <strong>Name::</strong>{{$value->cargo->name}}</br>
                                    <strong>Quantity::</strong>{{$value->cargo->quantity}}</br>
                                    <strong>Weight::</strong>{{$value->cargo->weight}}</br>
                                    <strong>Cost::</strong>{{$value->cargo->cost}}</br>
                                 @endforeach
                                 </td>
                                 <td>
                                    @if($transaction->delivery_company == "")
                                        {{"No Choose"}}
                                    @else
                                        <strong>Name::</strong>{{$transaction->delivery_company->name}}</br>
                                        <strong>Phone::</strong>{{$transaction->delivery_company->phone}}
                                    @endif
                                 </td>
                                 <td>
                                    <strong>Weight::</strong>{{$transaction->total_weight}}</br>
                                    <strong>Cost::</strong>{{$transaction->total_cost}}
                                 </td>
                                 <td>
                                     @if($transaction->status == 0)
                                     <div class="alert-danger" role="alert">{{"Not Confirmed"}}</div>
                                        
                                     @else
                                     <div class="alert-success" role="alert">{{"Confirmed"}}</div>
                                        
                                     @endif
                                 </td>
                                 <td>{{$transaction->created_at->diffForHumans()}}</td>
                                 @if ($user = Sentinel::getUser())    
                                    @if ($user->inRole('admin') || $user->inRole('deliverycompany'))
                                        <td>
                                            <div class="col-xs-1">                          
                                                 <a href="javascript:deleteUser('{{ $transaction->id }}');">
                                                    <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete"></i>
                                                 </a>
                                            </div>     
                                        </td>
                                    @endif
                                @endif
                             </tr>
                             @endforeach
                        </tbody>
                    </table>
                    {!! $transactions->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
    
@stop
@section('footer_scripts')
 <!-- begining of page level js -->
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.tableTools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/dataTables/dataTables.bootstrap.js') }}"></script>
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    </div>
  </div>
</div>
<script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>

<script type="text/javascript">
    $(function(){
        var table = $('#sample_editable_1').DataTable({
              "bInfo": false,
              "bPaginate": false,
               
          } );
        
    });

    function deleteUser(id) {
        if (confirm('Are you sure want to delete?')) {
        $.ajax({
            type: "DELETE",
            url: '/admin/shippingtransaction/' + id, //resource
            success: function() {
                    window.location = 'shippingtransaction';
                 
            }
        });
    }
}
</script>
@stop
 